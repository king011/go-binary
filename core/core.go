package core

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/types"
	"gitlab.com/king011/go-binary/utils"
	duk "gopkg.in/olebedev/go-duktape.v3"
)

// Core 核心系統
type Core struct {
	scripts *duk.Context
	context *types.Context
}

// New 創建 核心 系統
func New() (core *Core, e error) {
	scripts := duk.New()
	core = &Core{
		scripts: scripts,
	}
	e = core.native()
	if e != nil {
		core.scripts.DestroyHeap()
		core.scripts.Destroy()
	}
	core.context = types.NewContext()
	return
}

// Load 加載 檔案夾下的 定義
func (c *Core) Load(src string) (e error) {
	if filepath.IsAbs(src) {
		src = filepath.Clean(src)
	} else {
		src, e = filepath.Abs(src)
		if e != nil {
			return
		}
	}
	e = filepath.Walk(src, func(filename string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if src == filename {
			return nil
		}
		if strings.ToLower(filepath.Ext(filename)) != ".js" {
			return nil
		}
		b, e := ioutil.ReadFile(filename)
		if e != nil {
			return e
		}
		return c.eval(filename, b)
	})
	return
}
func (c *Core) eval(filename string, b []byte) (e error) {
	scripts := c.scripts

	scripts.PushString(filename)
	scripts.PutGlobalString("__filename")

	e = scripts.PevalString(utils.BytesToString(b))
	if e != nil {
		e = fmt.Errorf("%v [%s]", e, filename)
	}
	scripts.Pop()
	return
}

// Build 編譯
func (c *Core) Build(w io.Writer) (e error) {
	pkgs := c.context.Items()
	for _, pkg := range pkgs {
		ts := pkg.Items()
		if len(ts) == 0 {
			continue
		}
		fmt.Fprintln(w, "package :", pkg.Name())
		for _, t := range ts {
			enum := t.Enum()
			if enum == nil {
				fmt.Fprintf(w, "%7s : %s {\n", "type", t.Name())
				fields := t.Fields()
				items := fields.Items()
				for _, item := range items {
					symbol := item.Symbol

					var name string
					if symbol.IsCore() {
						name = fmt.Sprintf("# %s.%s", symbol.Package(), symbol.Name())
					} else {
						t, e = c.context.Find(symbol.Package(), symbol.Name())
						if e != nil {
							fmt.Println(t.Filename())
							e = types.SetErrorFilename(e, t.Filename())
							return
						}
						symbol.SetType(t)
						name = fmt.Sprintf("$ %s.%s", symbol.Package(), symbol.Name())
					}
					if item.Symbol.IsRepeat() {
						fmt.Printf("%7s      %5v %10s [ %s repeat ] %v\n", "", item.ID, item.Name, name, symbol.WriteString())
					} else {
						fmt.Printf("%7s      %5v %10s [ %s ] %v\n", "", item.ID, item.Name, name, symbol.WriteString())
					}
				}
			} else {
				fmt.Fprintf(w, "%7s : %s {\n", "enum", t.Name())
				items := enum.Items()
				for _, item := range items {
					fmt.Fprintf(w, "%7s      %v = %v\n", "", item.Name, item.Val)
				}
			}
			fmt.Fprintf(w, "%7s   }\n", "")
		}
	}
	return
}

// Handle 創建 代碼
func (c *Core) Handle(handlerID, root, dst string, clean bool) (e error) {
	h := handler.Find(handlerID)
	if h == nil {
		e = fmt.Errorf("not found handler : %v", handlerID)
		return
	}
	if clean {
		e = os.RemoveAll(dst)
		if e != nil {
			if !os.IsNotExist(e) {
				return
			}
		}
	}

	e = h.Handle(c.context, root, dst)
	return
}
