package handler

import (
	"fmt"
	"strings"

	"gitlab.com/king011/go-binary/core/types"
)

// Handler 處理器
type Handler interface {
	ID() string
	Handle(context *types.Context, root, dst string) (e error)
}

var _Handlers = make(map[string]Handler)

// Register 註冊 處理器
func Register(handler Handler) (e error) {
	id := strings.TrimSpace(handler.ID())
	if _, ok := _Handlers[id]; ok {
		e = fmt.Errorf("handler id already exists : %v", id)
		return
	}
	_Handlers[id] = handler
	return
}

// Find 查找 處理器
func Find(id string) Handler {
	return _Handlers[id]
}
