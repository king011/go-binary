package c

import (
	"os"
	"path/filepath"

	"gitlab.com/king011/go-binary/core/handler/c/names"
	"gitlab.com/king011/go-binary/core/types"
	"gitlab.com/king011/king-go/os/fileperm"
)

type _Codes struct {
	ctx *types.Context
	dst string
}

func (c _Codes) Handle() (e error) {
	pkgs := c.ctx.Items()
	for _, pkg := range pkgs {
		e = c.writePackage(pkg)
	}
	return
}
func (c _Codes) writePackage(pkg *types.Package) (e error) {
	items := pkg.Items()
	if len(items) == 0 {
		return
	}

	dst := filepath.Clean(c.dst + "/" + names.Filename(pkg.Name()))
	e = c.writeH(dst, pkg, items)
	if e != nil {
		return
	}
	e = c.writeC(dst, pkg, items)
	if e != nil {
		return
	}
	return
}

func (c _Codes) create(filename string) (f *os.File, e error) {
	dir := filepath.Dir(filename)
	e = os.MkdirAll(dir, fileperm.Directory)
	if e != nil {
		return
	}
	f, e = os.Create(filename)
	return
}
