package c

import (
	"fmt"
	"os"

	"gitlab.com/king011/go-binary/core/handler/c/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (c _Codes) writeEnum(f *os.File, pkg, name string, symbol types.EnumSymbol) (e error) {
	_, e = f.WriteString(fmt.Sprintf("	#define %s %v\n",
		names.Enum(pkg, name, symbol.Name),
		symbol.Val,
	))
	return
}
func (c _Codes) writeHInclude(f *os.File, pkg *types.Package, keys map[string]bool, field *types.Field) (ok bool, e error) {
	if field.Symbol.IsCore() {
		return
	}
	t, e := c.ctx.Find(field.Symbol.Package(), field.Symbol.Name())
	if e != nil {
		return
	}
	if t.Package().Name() == pkg.Name() {
		return
	}
	key := names.Include(t.Package().Name())
	if keys[key] {
		return
	}
	keys[key] = true
	_, e = f.WriteString(fmt.Sprintf("#include <%s>\n", key))
	if e != nil {
		return
	}
	ok = true
	return
}
func (c _Codes) writeHFcuntion(f *os.File, t *types.Type) (e error) {
	fields := t.Fields()
	if fields == nil {
		return
	}
	className := names.ClassName(t.Package().Name(), t.Name())
	_, e = f.WriteString(fmt.Sprintf(`	void binary_c_%s_reset(binary_c_%s_pt msg);
	void binary_c_%s_free(binary_c_free_f free, binary_c_%s_pt msg);
	int binary_c_%s_marshal_size(binary_c_context_pt ctx, binary_c_%s_pt msg, int use);
	int binary_c_%s_marshal(binary_c_context_pt ctx, binary_c_%s_pt msg, uint8_t *buffer, int capacity, int use);
	int binary_c_%s_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use);
	int binary_c_%s_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_%s_pt msg);
`,
		className, className,
		className, className,
		className, className,
		className, className,
		className,
		className, className,
	))
	if e != nil {
		return
	}

	items := fields.Items()

	for _, item := range items {
		symbol := item.Symbol
		if symbol.IsRepeat() {
			if symbol.IsCore() && symbol.Name() == "bool" {
				_, e = f.WriteString(fmt.Sprintf(`	int binary_c_%s_%s_set(binary_c_%s_pt msg, int i, BINARY_C_BOOL val);
	BINARY_C_BOOL binary_c_%s_%s_get(binary_c_%s_pt msg,int i);
`,
					className, names.FieldName(item.Name), className,
					className, names.FieldName(item.Name), className,
				))
			} else {
				valName := names.ValName(item.Symbol)
				_, e = f.WriteString(fmt.Sprintf(`	void binary_c_%s_%s_set(binary_c_%s_pt msg, int i, %s val);
	%s binary_c_%s_%s_get(binary_c_%s_pt msg, int i);
`,
					className, names.FieldName(item.Name), className, valName,
					valName, className, names.FieldName(item.Name), className,
				))
			}
		} else {
			tt := symbol.GetType()
			if tt == nil || tt.Enum() != nil {
				continue
			}
			ttClassName := names.ClassName(item.Symbol.Package(), item.Symbol.Name())
			_, e = f.WriteString(fmt.Sprintf(`	void binary_c_%s_%s_set(binary_c_%s_pt msg, binary_c_%s_pt val);
	binary_c_%s_pt binary_c_%s_%s_get(binary_c_%s_pt msg);
`,
				className, names.FieldName(item.Name), className, ttClassName,
				ttClassName, className, names.FieldName(item.Name), className,
			))
			if e != nil {
				return
			}
		}
	}

	_, e = f.WriteString("\n")
	if e != nil {
		return
	}
	return
}
func (c _Codes) writeHType(f *os.File, t *types.Type) (e error) {
	fields := t.Fields()
	if fields == nil {
		return
	}
	_, e = f.WriteString(fmt.Sprintf(`	typedef struct
	{
`))
	if e != nil {
		return
	}
	items := fields.Items()
	if len(items) == 0 {
		_, e = f.WriteString("		uint8_t __compatible_c;\n")
		if e != nil {
			return
		}
	} else {
		for _, item := range items {
			name := names.DeclareField(item.Symbol)
			_, e = f.WriteString(fmt.Sprintf(`		%s %s; // %v
`,
				name,
				names.FieldName(item.Name),
				item.ID,
			))
			if e != nil {
				return
			}
		}
	}
	className := names.ClassName(t.Package().Name(), t.Name())
	_, e = f.WriteString(fmt.Sprintf(`	} binary_c_%s_t, *binary_c_%s_pt;

`,
		className, className,
	))
	if e != nil {
		return
	}
	return
}
func (c _Codes) writeHIncludes(f *os.File, pkg *types.Package, ts []*types.Type) (e error) {
	count := len(ts)
	if count == 0 {
		return
	}
	keys := make(map[string]bool)
	for i := 0; i < count; i++ {
		t := ts[i]
		fields := t.Fields()
		if fields != nil {
			for _, item := range fields.Items() {
				_, e = c.writeHInclude(f, pkg, keys, item)
				if e != nil {
					return
				}
			}
		}
	}
	return
}
func (c _Codes) writeHEnums(f *os.File, ts []*types.Type) (e error) {
	count := len(ts)
	if count == 0 {
		return
	}
	for i := 0; i < len(ts); i++ {
		t := ts[i]
		enum := t.Enum()
		if enum != nil {
			items := enum.Items()
			for _, item := range items {
				e = c.writeEnum(f, t.Package().Name(), t.Name(), item)
				if e != nil {
					return
				}
			}

			className := names.ClassName(t.Package().Name(), t.Name())
			_, e = f.WriteString(fmt.Sprintf(`	const char* binary_c_%s_enum_string(uint16_t val);
	uint16_t binary_c_%s_enum_val(const char* str);


`,
				className,
				className,
			))
			if e != nil {
				return
			}
			return
		}
	}
	return
}
func (c _Codes) writeHTypes(f *os.File, ts []*types.Type) (e error) {
	count := len(ts)
	if count == 0 {
		return
	}
	for i := 0; i < len(ts); i++ {
		t := ts[i]
		e = c.writeHType(f, t)
		if e != nil {
			return
		}
	}
	for i := 0; i < len(ts); i++ {
		t := ts[i]
		e = c.writeHFcuntion(f, t)
		if e != nil {
			return
		}
	}
	return
}

func (c _Codes) writeH(dst string, pkg *types.Package, ts []*types.Type) (e error) {
	f, e := c.create(dst + ".h")
	if e != nil {
		return
	}
	defer f.Close()
	macro := names.Macro(pkg.Name())
	_, e = f.WriteString(
		fmt.Sprintf(`#ifndef %s
#define %s
#include <binary_c.h>
`,
			macro, macro,
		))
	if e != nil {
		return
	}

	// include
	e = c.writeHIncludes(f, pkg, ts)
	if e != nil {
		return
	}

	_, e = f.WriteString(
		`#ifdef __cplusplus
extern "C"
{
#endif

`)
	if e != nil {
		return
	}

	// 枚舉
	e = c.writeHEnums(f, ts)
	if e != nil {
		return
	}

	// class
	c.writeHTypes(f, ts)
	if e != nil {
		return
	}

	_, e = f.WriteString(
		fmt.Sprintf(`#ifdef __cplusplus
}
#endif

#endif // %s
`,
			macro,
		))
	if e != nil {
		return
	}
	return
}
