package c

import (
	"gitlab.com/king011/go-binary/core/handler/c/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (m *_Marshal) WriteCUnmarshalSizeFieldCore(index int, item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsString() {
		if symbol.IsRepeat() {
			e = m.WriteStringf(`		use = binary_c_unmarshal_%vs_size(ctx, &(fields[%v]), use);
		BINARY_C_CHECK_ERROR(use)
`,
				symbol.Name(), index,
			)
		} else {
			e = m.WriteStringf(`		use = binary_c_unmarshal_%v_size(&(fields[%v]), use);
		BINARY_C_CHECK_ERROR(use)
`,
				symbol.Name(), index,
			)
		}
	} else if symbol.IsRepeat() {
		e = m.WriteStringf(`		use = binary_c_unmarshal_%vs_size(&(fields[%v]), use);
		BINARY_C_CHECK_ERROR(use)
`,
			symbol.Name(), index,
		)
	}
	return
}
func (m *_Marshal) WriteCUnmarshalSizeFieldUser(index int, item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsEnum() {
		e = m.WriteStringf(`		use = binary_c_unmarshal_int16s_size(&(fields[%v]), use);
		BINARY_C_CHECK_ERROR(use)
`,
			index,
		)
	} else {
		className := names.ClassName(symbol.Package(), symbol.Name())
		if symbol.IsRepeat() {
			e = m.WriteStringf(`		use = binary_c_unmarshal_class_array_size(ctx, (binary_c_unmarshal_size_f)binary_c_%v_unmarshal_size, sizeof(binary_c_%v_t), &(fields[%v]), use);
		BINARY_C_CHECK_ERROR(use)
`,
				className, className, index,
			)
		} else {
			e = m.WriteStringf(`		use = binary_c_%v_unmarshal_size(ctx, fields[%v].data, fields[%v].length, use + sizeof(binary_c_%v_t));
		BINARY_C_CHECK_ERROR(use)
`,
				className, index, index, className,
			)
		}
	}
	return
}
func (m *_Marshal) WriteCUnmarshalSizeField(index int, item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = m.WriteCUnmarshalSizeFieldCore(index, item)
	} else {
		e = m.WriteCUnmarshalSizeFieldUser(index, item)
	}
	return
}

func (m *_Marshal) WriteCUnmarshalSizeFieldCount(items []*types.Field) (count int) {
	for _, item := range items {
		symbol := item.Symbol
		if symbol.IsRepeat() || symbol.IsString() || symbol.IsClass() {
			count++
		}
	}
	return
}
func (m *_Marshal) WriteCUnmarshalSize() (e error) {
	items := m.t.Fields().Items()
	count := m.WriteCUnmarshalSizeFieldCount(items)
	if count == 0 {
		e = m.WriteStringf(`int binary_c_%s_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use)
{
	return 0;
}
`,
			m.className,
		)
		if e != nil {
			return
		}
		return
	}

	e = m.WriteStringf(`int binary_c_%s_unmarshal_size(binary_c_context_pt ctx, uint8_t *buffer, int n, int use)
{
`,
		m.className,
	)
	if e != nil {
		return
	}
	e = m.WriteStringf(`	int ok = BINARY_C_OK;
	binary_c_field_t fields[%v] = {0};
`,
		count,
	)
	if e != nil {
		return
	}
	i := 0
	for _, item := range items {
		symbol := item.Symbol
		if symbol.IsRepeat() || symbol.IsClass() || symbol.IsString() {
			e = m.WriteStringf(`	fields[%v].id = %v;
`,
				i, item.ID,
			)
			if e != nil {
				return
			}
			i++
		}
	}

	e = m.WriteStringf(`	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, %v);
	BINARY_C_CHECK_ERROR(ok)
`,
		count,
	)
	i = 0
	for _, item := range items {
		symbol := item.Symbol
		if symbol.IsRepeat() || symbol.IsString() || symbol.IsClass() {

			e = m.WriteStringf(`	if (fields[%v].length)
	{
`,
				i,
			)
			if e != nil {
				return
			}
			e = m.WriteCUnmarshalSizeField(i, item)
			if e != nil {
				return
			}
			e = m.WriteStringf(`	}
`)
			if e != nil {
				return
			}
			i++
		}
	}

	e = m.WriteStringln(`	return use;
}`)
	if e != nil {
		return
	}
	return
}
