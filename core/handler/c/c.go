package c

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/handler/c/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (c _Codes) writeCReset(f *os.File, t *types.Type, className string) (e error) {
	_, e = f.WriteString(fmt.Sprintf(`void binary_c_%s_reset(binary_c_%s_pt msg)
{
	if (msg)
	{
		memset(msg, 0, sizeof(binary_c_%s_t));
	}
}
`,
		className, className, className,
	))
	if e != nil {
		return
	}
	return
}
func (c _Codes) writeCFreeField(f *os.File, item *types.Field, className string) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsString() {
		if symbol.IsRepeat() {
			_, e = f.WriteString(fmt.Sprintf(`			binary_c_free_strings(free, &(msg->%v));
`,
				fieldName,
			))
		} else {
			_, e = f.WriteString(fmt.Sprintf(`			binary_c_free_string(free, &(msg->%v));
`,
				fieldName,
			))
		}
	} else if symbol.IsClass() {
		if symbol.IsRepeat() {
			className := names.ClassName(symbol.Package(), symbol.Name())
			_, e = f.WriteString(fmt.Sprintf(`			binary_c_free_class_array((binary_c_free_class_f)binary_c_%v_free, free, &(msg->%v));
`,
				className, fieldName,
			))
		} else {
			className := names.ClassName(symbol.Package(), symbol.Name())
			_, e = f.WriteString(fmt.Sprintf(`			if (msg->%v)
			{
				binary_c_%v_free(free, msg->%v);
				free(msg->%v);
			}
`,
				fieldName,
				className, fieldName,
				fieldName,
			))
		}
	} else if symbol.IsRepeat() {
		if symbol.IsBool() {
			_, e = f.WriteString(fmt.Sprintf(`			binary_c_free_bits(free, &(msg->%v));
`,
				fieldName,
			))
		} else {
			_, e = f.WriteString(fmt.Sprintf(`			binary_c_free_array(free, &(msg->%v));
`,
				fieldName,
			))
		}
	}
	return
}
func (c _Codes) writeCFree(f *os.File, t *types.Type, className string) (e error) {
	_, e = f.WriteString(fmt.Sprintf(`void binary_c_%s_free(binary_c_free_f free, binary_c_%s_pt msg)
{
	if (msg)
	{
		if (free)
		{
`,
		className, className,
	))
	if e != nil {
		return
	}

	items := t.Fields().Items()
	for _, item := range items {
		e = c.writeCFreeField(f, item, className)
		if e != nil {
			return
		}
	}

	f.WriteString(fmt.Sprintf(`		}
		memset(msg, 0, sizeof(binary_c_%s_t));
	}
}
`,
		className,
	))
	return
}
func (c _Codes) writeCEnums(f *os.File, t *types.Type) (e error) {
	enum := t.Enum()
	if enum == nil {
		return
	}
	className := names.ClassName(t.Package().Name(), t.Name())
	// to string
	_, e = f.WriteString(fmt.Sprintf(`const char *binary_c_%s_enum_string(uint16_t val)
{
`,
		className,
	))
	if e != nil {
		return
	}

	_, e = f.WriteString(`	const char *str;
	switch (val)
	{
`)
	if e != nil {
		return
	}

	items := enum.Items()
	for _, item := range items {
		f.WriteString(fmt.Sprintf(`	case %v:
		str = "%s";
		break;
`,
			item.Val,
			item.NameString(),
		))
	}

	_, e = f.WriteString(`	default:
		str = "unknow";
		break;
	}
	return str;
}
`)
	if e != nil {
		return
	}

	// to val
	_, e = f.WriteString(fmt.Sprintf(`uint16_t binary_c_%s_enum_val(const char *str)
{
`,
		className,
	))
	if e != nil {
		return
	}

	_, e = f.WriteString(`	uint16_t val = 0;
`)
	if e != nil {
		return
	}

	for i, item := range items {
		var str string
		if i == 0 {
			str = "if"
		} else {
			str = "else if"
		}
		_, e = f.WriteString(fmt.Sprintf(`	%s (!strcmp(str, "%s"))
	{
		val = %v;
	}
`,
			str, item.NameString(),
			item.Val,
		))
		if e != nil {
			return
		}
	}

	_, e = f.WriteString(`	
	return val;
}
`)
	if e != nil {
		return
	}
	return
}
func (c _Codes) writeCType(f *os.File, t *types.Type) (e error) {
	fields := t.Fields()
	if fields == nil {
		return
	}
	className := names.ClassName(t.Package().Name(), t.Name())
	// reset
	e = c.writeCReset(f, t, className)
	if e != nil {
		return
	}
	e = c.writeCFree(f, t, className)
	if e != nil {
		return
	}

	// marshal
	m := &_Marshal{
		Writer: handler.Writer{
			W: f,
		},
		f:         f,
		t:         t,
		className: className,
	}
	e = m.WriteCSize()
	if e != nil {
		return
	}
	e = m.WriteCMarshal()
	if e != nil {
		return
	}

	// unmarshal
	e = m.WriteCUnmarshalSize()
	if e != nil {
		return
	}
	e = m.WriteCUnmarshal()
	if e != nil {
		return
	}

	items := fields.Items()
	for _, item := range items {
		symbol := item.Symbol
		fieldName := names.FieldName(item.Name)
		if symbol.IsRepeat() {
			if symbol.IsEnum() {
				_, e = f.WriteString(fmt.Sprintf(`void binary_c_%s_%s_set(binary_c_%s_pt msg,int i,int16_t val)
{
	((int16_t*)(msg->%s.data))[i] = val;
}
int16_t binary_c_%s_%s_get(binary_c_%s_pt msg,int i)
{
	return ((int16_t*)(msg->%s.data))[i];
}
`,
					className, fieldName, className,
					fieldName,

					className, fieldName, className,
					fieldName,
				))
				if e != nil {
					return
				}
			} else {
				if symbol.IsCore() && symbol.Name() == "bool" {
					_, e = f.WriteString(fmt.Sprintf(`int binary_c_%s_%s_set(binary_c_%s_pt msg,int i,BINARY_C_BOOL val)
{
	return binary_c_bits_set(&(msg->%s),i,val);
}
BINARY_C_BOOL binary_c_%s_%s_get(binary_c_%s_pt msg,int i)
{
	return binary_c_bits_get(&(msg->%s),i);
}
`,
						className, fieldName, className,
						fieldName,

						className, fieldName, className,
						fieldName,
					))
				} else {
					valName := names.ValName(item.Symbol)
					_, e = f.WriteString(fmt.Sprintf(`void binary_c_%s_%s_set(binary_c_%s_pt msg,int i,%s val)
{
	((%s*)(msg->%s.data))[i] = val;
}
%s binary_c_%s_%s_get(binary_c_%s_pt msg,int i)
{
	return ((%s*)(msg->%s.data))[i];
}
`,
						className, fieldName, className, valName,
						valName, fieldName,

						valName, className, fieldName, className,
						valName, fieldName,
					))
				}
				if e != nil {
					return
				}
			}
		} else {
			if symbol.IsClass() {
				valName := names.ValName(item.Symbol)
				_, e = f.WriteString(fmt.Sprintf(`void binary_c_%s_%s_set(binary_c_%s_pt msg,%s val)
{
	msg->%s = val;
}
%s binary_c_%s_%s_get(binary_c_%s_pt msg)
{
	return (%s)(msg->%s);
}
`,
					className, fieldName, className, valName,
					fieldName,

					valName, className, fieldName, className,
					valName, fieldName,
				))
				if e != nil {
					return
				}
			}
		}
	}

	_, e = f.WriteString("\n")
	if e != nil {
		return
	}
	return
}
func (c _Codes) writeC(dst string, pkg *types.Package, ts []*types.Type) (e error) {
	f, e := c.create(dst + ".c")
	if e != nil {
		return
	}
	defer f.Close()

	_, e = f.WriteString(
		fmt.Sprintf(`#include "%s.h"

`,
			filepath.Base(dst),
		))
	if e != nil {
		return
	}
	// 枚舉
	for i := 0; i < len(ts); i++ {
		e = c.writeCEnums(f, ts[i])
		if e != nil {
			return
		}
	}
	// class
	for i := 0; i < len(ts); i++ {
		e = c.writeCType(f, ts[i])
		if e != nil {
			return
		}
	}
	return
}
