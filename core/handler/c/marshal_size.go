package c

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler/c/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (m *_Marshal) WriteCSize() (e error) {
	_, e = m.f.WriteString(fmt.Sprintf(`int binary_c_%s_marshal_size(binary_c_context_pt ctx, binary_c_%s_pt msg, int use)
{
	if (!msg)
	{
		return use;
	}
`,
		m.className, m.className,
	))
	if e != nil {
		return
	}
	fields := m.t.Fields()
	items := fields.Items()
	for _, item := range items {
		e = m.cSizeField(item)
		if e != nil {
			return
		}
	}

	_, e = m.f.WriteString(`
	return use;
}
`)
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) cSizeFieldCore(item *types.Field, repeat bool) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	className := symbol.Name()
	if symbol.IsRepeat() {
		e = m.WriteStringf(`	use = binary_c_marshal_%vs_size(ctx, use, &(msg->%v));
	BINARY_C_CHECK_ERROR(use)
`,
			className, fieldName,
		)
	} else {
		if symbol.IsString() {
			e = m.WriteStringf(`	use = binary_c_marshal_%v_size(ctx, use, &(msg->%v));
	BINARY_C_CHECK_ERROR(use)
`,
				className, fieldName,
			)
		} else {
			e = m.WriteStringf(`	use = binary_c_marshal_%v_size(ctx, use, msg->%v);
	BINARY_C_CHECK_ERROR(use)
`,
				className, fieldName,
			)
		}
	}
	return
}
func (m *_Marshal) cSizeField(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = m.cSizeFieldCore(item, symbol.IsRepeat())
	} else {
		e = m.cSizeFieldUser(item, symbol.IsRepeat())
	}
	return
}
func (m *_Marshal) cSizeFieldUser(item *types.Field, repeat bool) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if repeat {
			e = m.WriteStringf(`	use = binary_c_marshal_int16s_size(ctx, use, &(msg->%v));
	BINARY_C_CHECK_ERROR(use)
`,
				fieldName,
			)
		} else {
			e = m.WriteStringf(`	use = binary_c_marshal_int16_size(ctx, use, msg->%v);
	BINARY_C_CHECK_ERROR(use)
`,
				fieldName,
			)
		}
	} else {
		className := names.ClassName(symbol.Package(), symbol.Name())
		if repeat {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (msg->%v.length)
	{
		use = binary_c_marshal_class_array_size(ctx, (binary_c_marshal_size_f)binary_c_%v_marshal_size, &(msg->%v), use);
		BINARY_C_CHECK_ERROR(use)
	}
`,
				fieldName,
				className, fieldName,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (msg->%v)
	{
		use = binary_c_marshal_size_add(ctx, use, 2 + 2);
		BINARY_C_CHECK_ERROR(use)
		use = binary_c_%v_marshal_size(ctx, msg->%v, use);
		BINARY_C_CHECK_ERROR(use)
	}
`,
				fieldName,
				className, fieldName,
			))
		}
	}
	return
}
