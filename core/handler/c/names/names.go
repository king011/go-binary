package names

import (
	"fmt"
	"strings"

	"gitlab.com/king011/go-binary/core/types"
)

// Macro 返回 宏 名
func Macro(pkg string) string {
	pkg = strings.ToUpper(pkgName(pkg))
	return fmt.Sprintf(`__BINARY_C_PROTOCOL_%s_H__`, pkg)
}

// Enum 返回 枚舉名
func Enum(pkg, class, field string) string {
	return "BINARY_C_" + strings.ToUpper(pkgName(pkg)+"_"+class+"_ENUM_"+field)
}

// Include 返回 include 路徑
func Include(pkg string) string {
	return Filename(pkg) + ".h"
}

// Filename 返回 檔案名 路徑
func Filename(pkg string) string {
	return strings.ToLower(pkg)
}
func pkgName(pkg string) string {
	return strings.ReplaceAll(pkg, "/", "_")
}

// ClassName 返回 型別名
func ClassName(pkg, name string) string {
	return strings.ToLower(pkgName(pkg) + "_" + name)
}

// FieldName 返回 屬性名稱
func FieldName(name string) string {
	return strings.ToLower(name)
}

// ValName 返回 型別名
func ValName(symbol types.Symbol) (name string) {
	if symbol.IsCore() {
		name = cName(symbol.Name())
	} else {
		if symbol.IsEnum() {
			name = "int16_t"
		} else {
			name = "binary_c_" + ClassName(symbol.Package(), symbol.Name()) + "_pt"
		}
	}
	return
}

// DeclareField 返回 聲明名稱
func DeclareField(symbol types.Symbol) (name string) {
	if symbol.IsRepeat() {
		if symbol.IsCore() && symbol.Name() == "bool" {
			name = "binary_c_bits_t"
		} else {
			name = "binary_c_array_t"
		}
	} else {
		if symbol.IsCore() {
			name = cName(symbol.Name())
		} else {
			if symbol.IsClass() {
				name = "binary_c_interface_t"
			} else {
				name = "int16_t"
			}
		}
	}
	return
}
func cName(name string) string {
	switch name {
	case "string":
		return "binary_c_string_t"
	case "float32":
		return "BINARY_C_FLOAT32_T"
	case "float64":
		return "BINARY_C_FLOAT64_T"
	case "byte":
		return "uint8_t"
	case "bool":
		return "BINARY_C_BOOL"
	}
	return name + "_t"
}
