package c

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler/c/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (m *_Marshal) writeCUnmarshalArray() (e error) {
	_, e = m.f.WriteString(fmt.Sprintf(`
int binary_c_%s_unmarshal_array(uint8_t *input, int n,binary_c_array_pt arrs)
{
	int ok = BINARY_C_OK;
	binary_c_array_t output;
	int count = binary_c_unmarshal_count(input, n);
	BINARY_C_CHECK_ERROR(count)
	output.capacity = count;
	output.length = count;
	output.data = BINARY_C_MALLOC(sizeof(binary_c_%s_pt) * count);
	if (output.data)
	{
		int i;
		binary_c_%s_pt data;
		for (i = 0; i < count; i++)
        {
			int data_size = (int)binary_c_uint16(input);
			if(data_size)
			{
				data = BINARY_C_MALLOC(sizeof(binary_c_%s_t));
				if(!data)
				{
					int j=0;
					for(;j<i;j++)
					{
						data = ((binary_c_%s_pt*)(output.data))[i];
						if(data)
						{
							binary_c_%s_free(data);
							BINARY_C_FREE(data);
						}
					}
					BINARY_C_FREE(output.data);
					return BINARY_C_ERROR_MALLOC;
				}
				binary_c_%s_reset(data);
				ok = binary_c_%s_unmarshal(input + 2,data_size,data);
				if(ok < 0)
				{
					int j=0;
					for(;j<i;j++)
					{
						data = ((binary_c_%s_pt*)(output.data))[i];
						if(data)
						{
							binary_c_%s_free(data);
							BINARY_C_FREE(data);
						}
					}
					BINARY_C_FREE(output.data);
					return ok;
				}
				((binary_c_%s_pt*)(output.data))[i] = data;
				input += 2 + data_size;
			}
			else
			{
				((binary_c_%s_pt*)(output.data))[i] = NULL;
				input += 2;
			}
		}
		*arrs = output;
	}
	else
	{
		ok = BINARY_C_ERROR_MALLOC;
	}
	return ok;
}
`,
		m.className, m.className,
		m.className, m.className, m.className,
		m.className, m.className, m.className, m.className,
		m.className,
		m.className, m.className,
	))
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) WriteCUnmarshal() (e error) {
	e = m.WriteStringf(`int binary_c_%s_unmarshal(binary_c_context_pt ctx, binary_c_allocator_pt allocator, uint8_t *buffer, int n, binary_c_%v_pt msg)
{
	int ok = BINARY_C_OK;
`,
		m.className, m.className,
	)
	if e != nil {
		return
	}

	items := m.t.Fields().Items()
	if len(items) != 0 {
		_, e = m.f.WriteString(fmt.Sprintf(`	int length = allocator->buffer.length;
	binary_c_field_t fields[%v];
	memset(fields, 0, sizeof(binary_c_field_t)*%v);
	binary_c_%s_reset(msg);
`,
			len(items), len(items),
			m.className,
		))
		if e != nil {
			return
		}
		for i, item := range items {
			_, e = m.f.WriteString(fmt.Sprintf("	fields[%v].id = %v;\n",
				i, item.ID,
			))
			if e != nil {
				return
			}
		}
		_, e = m.f.WriteString(fmt.Sprintf(`	ok = binary_c_unmarshal_fields(ctx, buffer, n, fields, %v);
	BINARY_C_CHECK_ERROR(ok)
`,
			len(items),
		))
		if e != nil {
			return
		}
		for i, item := range items {
			e = m.cUnmarshalField(i, item)
			if e != nil {
				return
			}
		}
	}
	_, e = m.f.WriteString(`	return ok;
}

`)
	if e != nil {
		return
	}

	return
}
func (m *_Marshal) cUnmarshalField(index int, item *types.Field) (e error) {
	_, e = m.f.WriteString(fmt.Sprintf(`	if (fields[%v].length)
	{
`,
		index,
	))
	if e != nil {
		return
	}

	symbol := item.Symbol
	if symbol.IsCore() {
		e = m.cUnmarshalFieldCore(index, item, symbol.IsRepeat())
	} else {
		e = m.cUnmarshalFieldUser(index, item, symbol.IsRepeat())
	}
	if e != nil {
		return
	}

	_, e = m.f.WriteString(fmt.Sprintf(`	}
`))
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) cUnmarshalFieldCore(index int, item *types.Field, repeat bool) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	switch symbol.Name() {
	case "byte":
		e = m.cUnmarshalFieldSize(index, item, fieldName, "uint8_t", 1, repeat)
	case "bool":
		if repeat {
			className := names.ClassName(m.t.Package().Name(), m.t.Name())
			_, e = m.f.WriteString(fmt.Sprintf(`		ok = binary_c_unmarshal_bits(ctx, allocator, fields[%v].data, fields[%v].length, &(msg->%v));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_%s_free, allocator, length, msg)
`,
				index, index,
				fieldName,
				className,
			))
		} else {
			e = m.cUnmarshalFieldSize(index, item, fieldName, "uint8_t", 1, repeat)
		}
	case "int16":
		fallthrough
	case "uint16":
		e = m.cUnmarshalFieldSize(index, item, fieldName, "uint16_t", 2, repeat)

	case "int32":
		fallthrough
	case "uint32":
		fallthrough
	case "float32":
		e = m.cUnmarshalFieldSize(index, item, fieldName, "uint32_t", 4, repeat)

	case "int64":
		fallthrough
	case "uint64":
		fallthrough
	case "float64":
		e = m.cUnmarshalFieldSize(index, item, fieldName, "uint64_t", 8, repeat)

	case "string":
		className := names.ClassName(m.t.Package().Name(), m.t.Name())
		if repeat {
			_, e = m.f.WriteString(fmt.Sprintf(`		ok = binary_c_unmarshal_strings(ctx, allocator, fields[%v].data, fields[%v].length, &(msg->%s));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_%s_free, allocator, length, msg)
`,
				index, index, fieldName,
				className,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`		ok = binary_c_unmarshal_string(ctx, allocator, fields[%v].data, fields[%v].length, &(msg->%s));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_%s_free, allocator, length, msg)
`,
				index, index, fieldName,
				className,
			))
		}
	}
	return
}
func (m *_Marshal) cUnmarshalFieldSize(index int, item *types.Field, fieldName, typeName string, val int, repeat bool) (e error) {
	className := names.ClassName(m.t.Package().Name(), m.t.Name())
	switch val {
	case 1:
		fallthrough
	case 2:
		fallthrough
	case 4:
		fallthrough
	case 8:
		if repeat {
			_, e = m.f.WriteString(fmt.Sprintf(`		ok = binary_c_unmarshal_array_bit(ctx, allocator, fields[%v].data, fields[%v].length, &(msg->%v), %v);
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_%s_free, allocator, length, msg)
`,
				index, index,
				fieldName, val,
				className,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`		ok = binary_c_unmarshal_bit%v(ctx, fields[%v].data, fields[%v].length, (%s *)&(msg->%v));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_%s_free, allocator, length, msg)
`,
				val*8, index, index,
				typeName, fieldName,
				className,
			))
		}
	default:
		e = fmt.Errorf("not support size : %v", val)
		return
	}
	if e != nil {
		return
	}
	return
}

func (m *_Marshal) cUnmarshalFieldUser(index int, item *types.Field, repeat bool) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		e = m.cUnmarshalFieldSize(index, item, fieldName, "uint16_t", 2, repeat)
	} else {
		className := names.ClassName(m.t.Package().Name(), m.t.Name())
		symbolName := names.ClassName(symbol.Package(), symbol.Name())
		if symbol.IsRepeat() {
			_, e = m.f.WriteString(fmt.Sprintf(`		ok = binary_c_unmarshal_class_array((binary_c_unmarshal_class_f)binary_c_%v_unmarshal, (binary_c_free_class_f)binary_c_%v_free, ctx, allocator, &fields[%v], &(msg->%v), sizeof(binary_c_%v_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_%s_free, allocator, length, msg)
`,
				symbolName, symbolName, index, fieldName, symbolName,
				className,
			))
		} else {
			e = m.WriteStringf(`		ok = binary_c_unmarshal_class((binary_c_unmarshal_class_f)binary_c_%v_unmarshal, ctx, allocator, &fields[%v], &(msg->%v), sizeof(binary_c_%v_t));
		BINARY_C_CHECK_ERROR_AND_FREE_CLASS(ok, binary_c_%s_free, allocator, length, msg)
`,
				symbolName, index, fieldName, symbolName,
				className,
			)
		}
	}
	return
}
