package c

import (
	"log"

	"gitlab.com/king011/go-binary/core/handler"
)

func init() {
	e := handler.Register(_Handler{})
	if e != nil {
		log.Fatalln(e)
	}
}
