package c

import (
	"fmt"
	"os"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/handler/c/names"

	"gitlab.com/king011/go-binary/core/types"
)

type _Marshal struct {
	handler.Writer
	f         *os.File
	t         *types.Type
	className string
}

func (m *_Marshal) WriteCMarshal() (e error) {
	_, e = m.f.WriteString(fmt.Sprintf(`int binary_c_%s_marshal(binary_c_context_pt ctx, binary_c_%s_pt msg, uint8_t *buffer, int capacity, int use)
{
	if(!msg)
	{
		return use;
	}
`,
		m.className, m.className,
	))
	if e != nil {
		return
	}

	fields := m.t.Fields()
	items := fields.Items()
	for _, item := range items {
		e = m.cField(item)
		if e != nil {
			return
		}
	}

	_, e = m.f.WriteString("	return use;\n}\n")
	if e != nil {
		return
	}
	return
}

func (m *_Marshal) cField(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = m.cFieldCore(item, symbol.IsRepeat())
	} else {
		e = m.cFieldUser(item, symbol.IsRepeat())
	}
	return
}
func (m *_Marshal) cFieldCore(item *types.Field, repeat bool) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	className := symbol.Name()
	if symbol.IsRepeat() {
		e = m.WriteStringf(`	use = binary_c_marshal_%vs(ctx, buffer, capacity, use, %v, &(msg->%v));
	BINARY_C_CHECK_ERROR(use)
`,
			className, item.ID, fieldName,
		)
	} else {
		if symbol.IsString() {
			e = m.WriteStringf(`	use = binary_c_marshal_%v(ctx, buffer, capacity, use, %v, &(msg->%v));
	BINARY_C_CHECK_ERROR(use)
`,
				className, item.ID, fieldName,
			)
		} else {
			e = m.WriteStringf(`	use = binary_c_marshal_%v(ctx, buffer, capacity, use, %v, msg->%v);
	BINARY_C_CHECK_ERROR(use)
`,
				className, item.ID, fieldName,
			)
		}
	}
	return
}
func (m *_Marshal) cFieldUser(item *types.Field, repeat bool) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if repeat {
			e = m.WriteStringf(`	use = binary_c_marshal_int16s(ctx, buffer, capacity, use, %v, &(msg->%v));
	BINARY_C_CHECK_ERROR(use)
`,
				item.ID, fieldName,
			)
		} else {
			e = m.WriteStringf(`	use = binary_c_marshal_int16(ctx, buffer, capacity, use, %v, msg->%v);
	BINARY_C_CHECK_ERROR(use)
`,
				item.ID, fieldName,
			)
		}
	} else {
		className := names.ClassName(symbol.Package(), symbol.Name())
		if repeat {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (msg->%v.length > 0)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_marshal_class_array(ctx, (binary_c_marshal_f)binary_c_%s_marshal, &(msg->%v), buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, %v, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
`,
				fieldName,
				className, fieldName,
				item.ID,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (msg->%v)
	{
		int start = use;
		use = binary_c_marshal_add(ctx, use, 2 + 2, capacity);
        BINARY_C_CHECK_ERROR(use)
		use = binary_c_%s_marshal(ctx, msg->%v, buffer, capacity, use);
		BINARY_C_CHECK_ERROR(use)
		BINARY_C_PUT_ID(ctx, buffer + start, %v, BINARY_C_WRITE_LENGTH)
        ctx->byte_order.put_uint16(buffer + start + 2, (uint16_t)(use - start - 4));
	}
`,
				fieldName,
				className, fieldName,
				item.ID,
			))
		}
	}
	return
}
