package golang

import (
	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/handler/golang/names"
	"gitlab.com/king011/go-binary/core/types"
)

type _Base struct {
	handler.Writer
	t         *types.Type
	className string
}

func (b _Base) Write() (e error) {
	e = b.WriteStringf(`
// Reset .
func (t *%v) Reset() {
`,
		b.className,
	)
	if e != nil {
		return
	}

	items := b.t.Fields().Items()
	for _, item := range items {
		e = b.writeItem(item)
		if e != nil {
			return
		}
	}

	e = b.WriteStringf(`}
`,
	)
	if e != nil {
		return
	}
	return
}
func (b _Base) writeItem(field *types.Field) (e error) {
	fieldName := names.FieldName(field.Name)
	symbol := field.Symbol
	if symbol.IsRepeat() {
		b.WriteStringf(`	t.%v = nil
`,
			fieldName,
		)
	} else {
		if symbol.IsCore() {
			if symbol.IsString() {
				b.WriteStringf(`	t.%v = ""
`,
					fieldName,
				)
			} else if symbol.IsBool() {
				b.WriteStringf(`	t.%v = false
`,
					fieldName,
				)
			} else {
				b.WriteStringf(`	t.%v = 0
`,
					fieldName,
				)
			}
		} else {
			if symbol.IsClass() {
				b.WriteStringf(`	t.%v = nil
`,
					fieldName,
				)
			} else {
				b.WriteStringf(`	t.%v = 0
`,
					fieldName,
				)
			}
		}
	}
	return
}
