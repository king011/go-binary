package golang

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"

	"gitlab.com/king011/go-binary/core/handler"

	"gitlab.com/king011/go-binary/core/handler/golang/names"
	"gitlab.com/king011/go-binary/core/types"
	"gitlab.com/king011/king-go/os/fileperm"
)

type _Codes struct {
	ctx  *types.Context
	dst  string
	root string
}

func (c _Codes) Handle() (e error) {
	pkgs := c.ctx.Items()
	for _, pkg := range pkgs {
		e = c.writePackage(pkg)
	}
	return
}
func (c _Codes) writePackage(pkg *types.Package) (e error) {
	items := pkg.Items()
	if len(items) == 0 {
		return
	}

	dir := c.dst + "/" + names.Directory(pkg.Name())
	keys := pkg.FileKeys()

	for key, items := range keys {
		if len(items) == 0 {
			return
		}
		filename := filepath.Base(key)
		ext := filepath.Ext(filename)
		filename = filepath.Clean(dir + "/" + filename[:len(filename)-len(ext)] + ".go")

		e = c.writeItem(pkg, items, c.root, filename)
		if e != nil {
			return
		}
	}
	return
}

func (c _Codes) create(filename string) (f *os.File, e error) {
	dir := filepath.Dir(filename)
	e = os.MkdirAll(dir, fileperm.Directory)
	if e != nil {
		return
	}
	f, e = os.Create(filename)
	return
}

func (c _Codes) writeItem(pkg *types.Package, items []*types.Type, root, filename string) (e error) {
	f, e := c.create(filename)
	if e != nil {
		return
	}
	defer f.Close()
	_, e = f.WriteString(fmt.Sprintf(`package %v
`,
		names.Package(pkg.Name()),
	))
	if e != nil {
		return
	}
	sort.Sort(types.SortType(items))
	class := &_Class{
		Writer: handler.Writer{W: f},
		root:   root,
		pkg:    pkg,
		items:  items,
	}
	e = class.Write()
	if e != nil {
		return
	}
	return
}
