package golang

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler/golang/names"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/types"
)

type _Class struct {
	handler.Writer
	root  string
	pkg   *types.Package
	items []*types.Type
}

func (c *_Class) Write() (e error) {
	e = c.WriteStringln(`
import (
	"gitlab.com/king011/binary-go/core"`)
	if e != nil {
		return
	}

	// import
	for _, item := range c.items {
		fields := item.Fields()
		if fields == nil {
			continue
		}
		e = c.writeImport(item, fields)
		if e != nil {
			return
		}
	}
	e = c.WriteStringln(`)`)
	if e != nil {
		return
	}

	// enum
	for _, item := range c.items {
		enum := item.Enum()
		if enum == nil {
			continue
		}
		e = c.writeEnum(item, enum)
		if e != nil {
			return
		}
	}

	// class
	for _, item := range c.items {
		fields := item.Fields()
		if fields == nil {
			continue
		}
		e = c.writeClass(item, fields)
		if e != nil {
			return
		}
	}
	return
}
func (c *_Class) writeEnum(t *types.Type, enum *types.Enum) (e error) {
	className := names.ClassName(t.Name())
	e = c.WriteStringf(`
// %v .
type %v int16
`,
		className,
		className,
	)
	if e != nil {
		return
	}
	items := enum.Items()
	if len(items) != 0 {
		e = c.WriteStringln(`
const (`)
		if e != nil {
			return
		}
		for i := 0; i < len(items); i++ {
			fieldName := names.FieldName(items[i].Name)

			e = c.WriteStringf(`	// %v%v %v
	%v%v = %v(%v)	
`,
				className, fieldName, items[i].NameString(),
				className, fieldName, className, items[i].Val,
			)
			if e != nil {
				return
			}
		}
		e = c.WriteStringln(")")
		if e != nil {
			return
		}
	}

	// String
	e = c.WriteStringf(`
// String .
func (v %v) String() (str string) {
	switch v {
`,
		className,
	)
	if e != nil {
		return
	}

	for i := 0; i < len(items); i++ {
		fieldName := names.FieldName(items[i].Name)
		e = c.WriteStringf(`	case %v%v:
		str = "%v"	
`,
			className, fieldName, items[i].NameString(),
		)
		if e != nil {
			return
		}
	}

	e = c.WriteString(`	default:
		str = "unknow"
	}
	return
}
`,
	)
	if e != nil {
		return
	}

	// New
	e = c.WriteStringf(`
// New%v .
func New%v(str string) (v %v) {
	switch str {
`,
		className, className, className,
	)
	if e != nil {
		return
	}

	for i := 0; i < len(items); i++ {
		fieldName := names.FieldName(items[i].Name)
		e = c.WriteStringf(`	case "%v":
		v = %v%v	
`,
			items[i].NameString(), className, fieldName,
		)
		if e != nil {
			return
		}
	}

	e = c.WriteString(`	}
	return
}
`,
	)
	if e != nil {
		return
	}

	m := &_Marshal{
		Writer: handler.Writer{
			W: c.W,
		},
		t:         t,
		className: className,
	}
	e = m.WriteEnum()
	if e != nil {
		return
	}
	return
}

func (c *_Class) writeImport(t *types.Type, fields *types.Fields) (e error) {
	keys := make(map[string]bool)
	items := fields.Items()
	for _, item := range items {
		symbol := item.Symbol
		if symbol.IsClass() {
			if symbol.Package() == t.Package().Name() {
				continue
			}

			importName := names.ImportName(symbol.Package())
			if keys[importName] {
				continue
			}
			keys[importName] = true
			importPath := names.ImportPath(c.root, symbol.Package())
			e = c.WriteStringf(`	%v "%v"
`,
				importName, importPath,
			)
			if e != nil {
				return
			}
		}
	}

	return
}
func (c *_Class) writeClass(t *types.Type, fields *types.Fields) (e error) {
	e = c.writeClassType(t, fields)
	if e != nil {
		return
	}

	className := names.ClassName(t.Name())
	b := &_Base{
		Writer: handler.Writer{
			W: c.W,
		},
		t:         t,
		className: className,
	}
	e = b.Write()
	if e != nil {
		return
	}

	m := &_Marshal{
		Writer: handler.Writer{
			W: c.W,
		},
		t:         t,
		className: className,
	}
	e = m.Write()
	if e != nil {
		return
	}
	return
}
func (c *_Class) writeClassType(t *types.Type, fields *types.Fields) (e error) {
	className := names.ClassName(t.Name())
	e = c.WriteStringf(fmt.Sprintf(`
// %v .
type %v struct {
`,
		className, className,
	))
	if e != nil {
		return
	}

	items := fields.Items()
	for _, item := range items {
		e = c.writeField(item)
		if e != nil {
			return
		}
	}

	e = c.WriteStringln("}")
	if e != nil {
		return
	}
	return
}
func (c *_Class) writeField(field *types.Field) (e error) {
	symbol := field.Symbol
	if symbol.IsCore() {
		e = c.writeFieldCore(field)
	} else {
		e = c.writeFieldUser(field)
	}
	return
}
func (c *_Class) writeFieldUser(field *types.Field) (e error) {
	symbol := field.Symbol
	fieldName := names.FieldName(field.Name)
	className := names.ValName(symbol, c.pkg.Name())
	if symbol.IsEnum() {
		if symbol.IsRepeat() {
			e = c.WriteStringf(`	%v	[]%v
`,
				fieldName, className,
			)
		} else {
			e = c.WriteStringf(`	%v	%v
`,
				fieldName, className,
			)
		}
	} else if symbol.IsRepeat() {
		e = c.WriteStringf(`	%v	[]*%v
`,
			fieldName, className,
		)
	} else {
		e = c.WriteStringf(`	%v	*%v
`,
			fieldName, className,
		)
	}
	return
}

func (c *_Class) writeFieldCore(field *types.Field) (e error) {
	symbol := field.Symbol
	fieldName := names.FieldName(field.Name)
	className := names.ValName(symbol, c.pkg.Name())
	if symbol.IsRepeat() {
		e = c.WriteStringf(`	%v	[]%v
`,
			fieldName, className,
		)
	} else {
		e = c.WriteStringf(`	%v	%v
`,
			fieldName, className,
		)
	}
	return
}
