package golang

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler/golang/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (m *_Marshal) unmarshal() (e error) {
	e = m.unmarshalArray()
	if e != nil {
		return
	}

	items := m.t.Fields().Items()
	if len(items) == 0 {
		e = m.WriteStringf(`
// Unmarshal .
func (t *%v) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	return
}
`,
			m.className,
		)
		return
	}

	e = m.WriteStringf(`
// Unmarshal .
func (t *%v) Unmarshal(ctx core.Context, buffer []byte) (e error) {
	var fields [%v]core.Field
`,
		m.className, len(items),
	)
	if e != nil {
		return
	}
	for i, item := range items {
		e = m.WriteStringf(`	fields[%v].ID = %v
`, i, item.ID)
		if e != nil {
			return
		}
	}
	e = m.WriteStringln(`	e = core.UnmarshalFields(ctx, buffer, fields[:])
	if e != nil {
		return
	}`)
	if e != nil {
		return
	}

	for i, item := range items {
		e = m.unmarshalField(item, i)
		if e != nil {
			return
		}
	}

	e = m.WriteStringln(`
	return
}`,
	)
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) unmarshalArray() (e error) {
	e = m.WriteStringf(`
// Unmarshal0%vs .
func Unmarshal0%vs(ctx core.Context, buffer []byte) (arrs []*%v, e error) {
	items, e := core.UnmarshalCount(ctx, buffer)
	if e != nil || len(items) == 0 {
		return
	}
	arrs = make([]*%v, len(items))
	for i, item := range items {
		if len(item) != 0 {
			var tmp %v
			e = tmp.Unmarshal(ctx, item)
			if e != nil {
				arrs = nil
				return
			}
			arrs[i] = &tmp
		}
	}
	return
}
`,
		m.className,
		m.className, m.className,
		m.className, m.className,
	)
	if e != nil {
		return
	}

	return
}
func (m *_Marshal) unmarshalFieldCore(item *types.Field, index int) (e error) {
	symbol := item.Symbol
	className := names.ClassName(symbol.Name())
	fieldName := names.FieldName(item.Name)
	if symbol.IsRepeat() {
		if symbol.IsString() {
			e = m.WriteStringf(`	t.%v, e = core.Unmarshal%vs(ctx, fields[%v])
	if e != nil {
		return
	}
`,
				fieldName,
				className, index,
			)
		} else {
			e = m.WriteStringf(`	t.%v = core.Unmarshal%vs(ctx, fields[%v])
`,
				fieldName,
				className, index,
			)
		}
	} else {
		e = m.WriteStringf(`	t.%v = core.Unmarshal%v(ctx, fields[%v])
`,
			fieldName,
			className, index,
		)
	}
	return
}
func (m *_Marshal) unmarshalFieldUser(item *types.Field, index int) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if symbol.IsRepeat() {
			var funcName string
			if symbol.Package() == m.t.Package().Name() {
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("Unmarshal0%vs", className)
			} else {
				importName := names.ImportName(symbol.Package())
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("%v.Unmarshal0%vs", importName, className)
			}
			e = m.WriteStringf(`	t.%v = %v(ctx, fields[%v].Buffer)
`,
				fieldName,
				funcName, index,
			)
		} else {
			className := names.ValName(symbol, m.className)
			e = m.WriteStringf(`	t.%v = %v(core.UnmarshalInt16(ctx, fields[%v]))
`,
				fieldName,
				className, index,
			)
		}
	} else {
		if symbol.IsRepeat() {
			var funcName string
			if symbol.Package() == m.t.Package().Name() {
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("Unmarshal0%vs", className)
			} else {
				importName := names.ImportName(symbol.Package())
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("%v.Unmarshal0%vs", importName, className)
			}
			e = m.WriteStringf(`	t.%v, e = %v(ctx, fields[%v].Buffer)
	if e != nil {
		return
	}
`,
				fieldName,
				funcName, index,
			)
		} else {
			var className string
			if symbol.Package() == m.t.Package().Name() {
				className = names.ClassName(symbol.Name())
			} else {
				importName := names.ImportName(symbol.Package())
				className = importName + "." + names.ClassName(symbol.Name())
			}
			e = m.WriteStringf(`	if len(fields[%v].Buffer) == 0 {
		t.%v = nil
	} else {
		var tmp %v
		e = tmp.Unmarshal(ctx, fields[%v].Buffer)
		if e != nil {
			return
		}
		t.%v = &tmp
	}
`,
				index,
				fieldName,
				className, index,
				fieldName,
			)
		}
	}
	return
}
func (m *_Marshal) unmarshalField(item *types.Field, index int) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = m.unmarshalFieldCore(item, index)
	} else {
		e = m.unmarshalFieldUser(item, index)
	}
	if e != nil {
		return
	}
	return
}
