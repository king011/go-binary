package golang

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler/golang/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (m *_Marshal) marshalSizeFieldUser(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if symbol.IsRepeat() {
			e = m.WriteStringf(`	if len(t.%v) != 0 {
		use, e = core.MarshalSizeAdd(ctx, use, 2+2+len(t.%v)*2)
		if e != nil {
			return
		}
	}
`,
				fieldName, fieldName,
			)
		} else {
			e = m.WriteStringf(`	if t.%v != 0 {
		use, e = core.MarshalSizeAdd(ctx, use, 2+2)
		if e != nil {
			return
		}
	}
`,
				fieldName,
			)
		}
	} else {
		if symbol.IsRepeat() {
			var funcName string
			if symbol.Package() == m.t.Package().Name() {
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("Marshal0Size%vs", className)
			} else {
				importName := names.ImportName(symbol.Package())
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("%v.Marshal0Size%vs", importName, className)
			}
			e = m.WriteStringf(`	use, e = %v(ctx, use, t.%v)
	if e != nil {
		return 0, e
	}
`,
				funcName, fieldName,
			)
		} else {
			e = m.WriteStringf(`	if t.%v != nil {
		use, e = core.MarshalTypeSize(ctx, use, t.%v)
		if e != nil {
			return
		}
	}
`,
				fieldName, fieldName,
			)

		}
	}
	return
}
func (m *_Marshal) marshalSizeFieldCore(item *types.Field) (e error) {
	symbol := item.Symbol
	className := names.ClassName(symbol.Name())
	fieldName := names.FieldName(item.Name)
	if symbol.IsRepeat() {
		e = m.WriteStringf(`	use, e = core.Marshal%vsSize(ctx, use, t.%v)
	if e != nil {
		return
	}
`,
			className, fieldName,
		)
	} else {
		e = m.WriteStringf(`	use, e = core.Marshal%vSize(ctx, use, t.%v)
	if e != nil {
		return
	}
`,
			className, fieldName,
		)

	}
	return
}
func (m *_Marshal) marshalSizeField(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = m.marshalSizeFieldCore(item)
	} else {
		e = m.marshalSizeFieldUser(item)
	}
	return
}
func (m *_Marshal) marshalSizeFunc() (e error) {
	funcName := fmt.Sprintf("Marshal0Size%vs", m.className)
	m.WriteStringf(`
// %v .
func %v(ctx core.Context, use int, arrs []*%v) (int, error) {
	if len(arrs) != 0 {
		var e error
		use, e = core.MarshalSizeAdd(ctx, use, 4)
		if e != nil {
			return 0, e
		}
		for _, v := range arrs {
			use, e = core.MarshalSizeAdd(ctx, use, 2)
			if e != nil {
				return 0, e
			}
			if v != nil {
				use, e = v.MarshalSize(ctx, use)
				if e != nil {
					return 0, e
				}
			}
		}
	}
	return use, nil
}
`,
		funcName, funcName, m.className,
	)

	return
}
func (m *_Marshal) marshalSize() (e error) {
	e = m.marshalSizeFunc()
	if e != nil {
		return
	}
	e = m.WriteStringf(`
// MarshalSize .
func (t *%v) MarshalSize(ctx core.Context, use int) (size int, e error) {
`,
		m.className,
	)
	if e != nil {
		return
	}

	items := m.t.Fields().Items()
	for _, item := range items {
		m.marshalSizeField(item)
	}

	e = m.WriteStringf(`	size = use
	return
}
`)
	if e != nil {
		return
	}
	return
}
