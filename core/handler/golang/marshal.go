package golang

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/handler/golang/names"
	"gitlab.com/king011/go-binary/core/types"
)

type _Marshal struct {
	handler.Writer
	t         *types.Type
	className string
}

func (m *_Marshal) Write() (e error) {
	e = m.marshalSize()
	if e != nil {
		return
	}
	e = m.marshal()
	if e != nil {
		return
	}
	e = m.unmarshal()
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) WriteEnum() (e error) {
	e = m.marshalEnum()
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) marshalEnum() (e error) {
	e = m.WriteStringf(`
// Marshal0%vs .
func Marshal0%vs(ctx core.Context, buffer []byte, use int, id uint16, arrs []%v) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	capacity := len(buffer)
	buffer = buffer[use:]
	valSize := 2
	arrsSize := len(arrs) * valSize
	use, e = core.MarshalAdd(ctx, use, 4+arrsSize, capacity)
	if e != nil {
		return
	}

	order := ctx.ByteOrder()
	core.PutID(order, buffer, id, core.BinaryWriteLength)
	order.PutUint16(buffer[2:], uint16(arrsSize))
	buffer = buffer[4:]
	for i := 0; i < len(arrs); i++ {
		order.PutUint16(buffer, uint16(arrs[i]))
		buffer = buffer[valSize:]
	}
	size = use
	return
}
`,
		m.className,
		m.className, m.className,
	)
	if e != nil {
		return
	}

	e = m.WriteStringf(`
// Unmarshal0%vs .
func Unmarshal0%vs(ctx core.Context, buffer []byte) (arrs []%v) {
	count := len(buffer)
	if count == 0 {
		return
	}
	count /= 2
	order := ctx.ByteOrder()
	arrs = make([]%v, count)
	for i := 0; i < count; i++ {
		arrs[i] = %v(order.Uint16(buffer[i*2:]))
	}
	return
}
`,
		m.className,
		m.className, m.className,
		m.className, m.className,
	)
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) marshalArray() (e error) {
	e = m.WriteStringf(`
// Marshal0%vs .
func Marshal0%vs(ctx core.Context, buffer []byte, use int, arrs []*%v) (size int, e error) {
	if len(arrs) == 0 {
		size = use
		return
	}
	var start int
	order := ctx.ByteOrder()
	for _, v:=range arrs {
		start = use
		use, e = core.MarshalAdd(ctx, use, 2, len(buffer))
		if e != nil {
			return
		}
		if v == nil {
			order.PutUint16(buffer[start:], 0)
		} else {
			use, e = v.MarshalToBuffer(ctx, buffer, use)
			if e != nil {
				return
			}
			order.PutUint16(buffer[start:], uint16(use-start-2))
		}
	}
	size = use
	return
}
`,
		m.className,
		m.className, m.className,
	)
	if e != nil {
		return
	}

	return
}
func (m *_Marshal) marshal() (e error) {
	e = m.marshalArray()
	if e != nil {
		return
	}
	e = m.WriteStringf(`
// Marshal .
func (t *%v) Marshal(ctx core.Context) (buffer []byte, e error) {
	size, e := t.MarshalSize(ctx, 0)
	if e != nil {
		return
	}
	if size == 0 {
		return
	}
	buffer = make([]byte, size)
	_, e = t.MarshalToBuffer(ctx, buffer, 0)
	if e != nil {
		buffer = nil
		return
	}
	return
}

// MarshalToBuffer .
func (t *%v) MarshalToBuffer(ctx core.Context, buffer []byte, use int) (size int, e error) {
`,
		m.className, m.className,
	)
	if e != nil {
		return
	}

	items := m.t.Fields().Items()
	for _, item := range items {
		e = m.marshalField(item)
		if e != nil {
			return
		}
	}

	e = m.WriteStringf(`	size = use
	return
}
`)
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) marshalField(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = m.marshalFieldCore(item)
	} else {
		e = m.marshalFieldUser(item)
	}
	return
}
func (m *_Marshal) marshalFieldUser(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if symbol.IsRepeat() {
			var funcName string
			if symbol.Package() == m.t.Package().Name() {
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("Marshal0%vs", className)
			} else {
				importName := names.ImportName(symbol.Package())
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("%v.Marshal0%vs", importName, className)
			}
			e = m.WriteStringf(`	if len(t.%v) != 0 {
		use, e = %v(ctx, buffer, use, %v, t.%v)
		if e != nil {
			return
		}
	}
`,
				fieldName,
				funcName, item.ID, fieldName,
			)
		} else {
			e = m.WriteStringf(`	use, e = core.MarshalInt16(ctx, buffer, use, %v, int16(t.%v))
	if e != nil {
		return
	}
`,
				item.ID, fieldName,
			)
		}
	} else {
		if symbol.IsRepeat() {
			var funcName string
			if symbol.Package() == m.t.Package().Name() {
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("Marshal0%vs", className)
			} else {
				importName := names.ImportName(symbol.Package())
				className := names.ClassName(symbol.Name())
				funcName = fmt.Sprintf("%v.Marshal0%vs", importName, className)
			}
			e = m.WriteStringf(`	if len(t.%v) != 0 {
		start := use
		use, e = core.MarshalAdd(ctx, use, 2+2, len(buffer))
		if e != nil {
			return
		}
		use, e = %v(ctx, buffer, use, t.%v)
		if e != nil {
			return
		}
		order := ctx.ByteOrder()
		core.PutID(order, buffer[start:], %v, core.BinaryWriteLength)
		order.PutUint16(buffer[start+2:], uint16(use-start-4))
	}
`,
				fieldName,
				funcName, fieldName,
				item.ID,
			)
		} else {
			e = m.WriteStringf(`	if t.%v != nil {
		start := use
		use, e = core.MarshalAdd(ctx, use, 2+2, len(buffer))
		if e != nil {
			return
		}
		use, e = t.%v.MarshalToBuffer(ctx, buffer, use)
		if e != nil {
			return
		}
		order := ctx.ByteOrder()
		core.PutID(order, buffer[start:], %v, core.BinaryWriteLength)
		order.PutUint16(buffer[start+2:], uint16(use-start-4))
	}
`,
				fieldName, fieldName,
				item.ID,
			)
		}
	}
	return
}
func (m *_Marshal) marshalFieldCore(item *types.Field) (e error) {
	symbol := item.Symbol
	className := names.ClassName(symbol.Name())
	fieldName := names.FieldName(item.Name)
	if symbol.IsRepeat() {
		e = m.WriteStringf(`	use, e = core.Marshal%vs(ctx, buffer, use, %v, t.%v)
	if e != nil {
		return
	}
`,
			className,
			item.ID, fieldName,
		)
	} else {
		e = m.WriteStringf(`	use, e = core.Marshal%v(ctx, buffer, use, %v, t.%v)
	if e != nil {
		return
	}
`,
			className,
			item.ID, fieldName,
		)
	}
	return
}
