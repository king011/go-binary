package golang

import (
	"strings"

	"gitlab.com/king011/go-binary/core/types"
)

// ID 處理器 id
const ID = "golang"

type _Handler struct {
}

func (_Handler) ID() string {
	return ID
}
func (_Handler) Handle(ctx *types.Context, root, dst string) (e error) {
	for strings.HasPrefix(root, "/") {
		root = root[1:]
	}
	for strings.HasSuffix(root, "/") {
		root = root[:len(root)-1]
	}
	codes := _Codes{
		ctx:  ctx,
		dst:  dst,
		root: root,
	}
	e = codes.Handle()
	return
}
