package names

import (
	"path/filepath"
	"strings"

	"gitlab.com/king011/go-binary/core/types"
)

func toUpper(str string) string {
	switch str {
	case "id":
		str = "ID"
	case "json":
		str = "JSON"
	case "http":
		str = "HTTP"
	case "https":
		str = "HTTPS"
	default:
		str = strings.ToUpper(str[:1]) + str[1:]
	}
	return str
}

// Directory 返回 檔案夾 路徑
func Directory(pkg string) string {
	return strings.ToLower(pkg)
}

// Package 返回 包名
func Package(pkg string) string {
	return strings.ToLower(filepath.Base(pkg))
}

// ImportPath 返回 import 路徑
func ImportPath(root, pkg string) string {
	return filepath.Clean(root + "/" + strings.ToLower(pkg))
}

// ClassName 返回 型別名
func ClassName(name string) string {
	strs := strings.Split(strings.ToLower(name), "_")
	for i, str := range strs {
		strs[i] = toUpper(str)
	}
	return strings.Join(strs, "")
}

// FieldName 返回 屬性名稱
func FieldName(name string) string {
	return ClassName(name)
}

// ImportName import 後的包名
func ImportName(pkg string) (name string) {
	return strings.ReplaceAll(strings.ToLower(pkg), "/", "_")
}

// ValName 返回 型別名
func ValName(symbol types.Symbol, pkg string) (name string) {
	if symbol.IsCore() {
		name = ccName(symbol.Name())
	} else {
		if symbol.Package() == pkg {
			name = ClassName(symbol.Name())
		} else {
			name = ImportName(symbol.Package()) + "." + ClassName(symbol.Name())
		}
	}
	return
}
func ccName(name string) string {
	return name
}
