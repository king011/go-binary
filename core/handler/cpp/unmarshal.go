package cpp

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler/cpp/names"

	"gitlab.com/king011/go-binary/core/types"
)

func (m *_Marshal) unmarshalFieldUser(index int, item *types.Field, repeat bool) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if repeat {
			_, e = m.f.WriteString(fmt.Sprintf(`			binarycpp::unmarshal_enums(ctx, fields[%v], %v);
`,
				index, fieldName,
			))
		} else {
			className := names.ValName(symbol)
			_, e = m.f.WriteString(fmt.Sprintf(`			%v = (%s)binarycpp::unmarshal_enum(ctx, fields[%v]);
`,
				fieldName, className, index,
			))
		}
	} else {
		if repeat {
			_, e = m.f.WriteString(fmt.Sprintf(`			binarycpp::unmarshal_types(ctx, fields[%v], %s);
`,
				index, fieldName,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`			binarycpp::unmarshal_type(ctx, fields[%v], %v);
`,
				index, fieldName,
			))
		}
	}
	return
}
func (m *_Marshal) unmarshalField(index int, item *types.Field) (e error) {
	_, e = m.f.WriteString(fmt.Sprintf(`		if (fields[%v].length)
		{
`,
		index,
	))
	if e != nil {
		return
	}

	symbol := item.Symbol
	if symbol.IsCore() {
		fieldName := names.FieldName(item.Name)
		_, e = m.f.WriteString(fmt.Sprintf(`			binarycpp::unmarshal(ctx, fields[%v], %v);
`,
			index,
			fieldName,
		))
	} else {
		e = m.unmarshalFieldUser(index, item, symbol.IsRepeat())
	}

	_, e = m.f.WriteString(fmt.Sprintf(`		}
`))
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) unmarshal() (e error) {
	items := m.t.Fields().Items()
	if len(items) == 0 {
		_, e = m.f.WriteString(fmt.Sprintf(`void %s_t::unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n)
{
}
`,
			m.className,
		))
		return
	}
	_, e = m.f.WriteString(fmt.Sprintf(`void %s_t::unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n)
{
	reset();
	try
	{
`,
		m.className,
	))
	if e != nil {
		return
	}

	_, e = m.f.WriteString(fmt.Sprintf(`		binarycpp::field_t fields[%v];
		memset(fields, 0, sizeof(binarycpp::field_t) * %v);
`,
		len(items), len(items),
	))
	if e != nil {
		return
	}
	for i, item := range items {
		_, e = m.f.WriteString(fmt.Sprintf("		fields[%v].id = %v;\n",
			i, item.ID,
		))
		if e != nil {
			return
		}
	}
	_, e = m.f.WriteString(fmt.Sprintf(`		binarycpp::unmarshal_fields(ctx, input, n, fields, %v);
`,
		len(items),
	))
	if e != nil {
		return
	}
	for i, item := range items {
		e = m.unmarshalField(i, item)
		if e != nil {
			return
		}
	}

	_, e = m.f.WriteString(`	}
	BINARY_CPP_CATCH_RESET
}
`)
	if e != nil {
		return
	}
	return
}
