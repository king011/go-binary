package cpp

import (
	"gitlab.com/king011/go-binary/core/types"
)

// ID 處理器 id
const ID = "cpp"

type _Handler struct {
}

func (_Handler) ID() string {
	return ID
}
func (_Handler) Handle(ctx *types.Context, root, dst string) (e error) {
	codes := _Codes{
		ctx: ctx,
		dst: dst,
	}
	e = codes.Handle()
	return
}
