package names

import (
	"fmt"
	"strings"

	"gitlab.com/king011/go-binary/core/types"
)

// Filename 返回 檔案名 路徑
func Filename(pkg string) string {
	return strings.ToLower(pkg)
}

// Namespace 返回 名字 空間
func Namespace(pkg string) string {
	return strings.ToLower(pkgName(pkg))
}
func pkgName(pkg string) string {
	return strings.ReplaceAll(pkg, "/", "::")
}

// Include 返回 include 路徑
func Include(pkg string) string {
	return Filename(pkg) + ".h"
}

// ClassName 返回 型別名
func ClassName(name string) string {
	return strings.ToLower(name)
}

// FieldName 返回 屬性名稱
func FieldName(name string) string {
	return strings.ToLower(name) + "_"
}

// FullClassNameByType 返回 型別名
func FullClassNameByType(t *types.Type) (name string) {
	name = fmt.Sprintf("%s::%s_t", pkgName(t.Package().Name()), ClassName(t.Name()))
	return
}

// ValName 返回 型別名
func ValName(symbol types.Symbol) (name string) {
	if symbol.IsCore() {
		name = ccName(symbol.Name())
	} else {
		name = fmt.Sprintf("%s::%s_t", pkgName(symbol.Package()), ClassName(symbol.Name()))
	}
	return
}
func ccName(name string) string {
	switch name {
	case "string":
		return "std::string"
	case "float32":
		return "BINARY_CPP_FLOAT32_T"
	case "float64":
		return "BINARY_CPP_FLOAT64_T"
	case "bool":
		return name
	case "byte":
		return "std::uint8_t"
	}
	return "std::" + name + "_t"
}
