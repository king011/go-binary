package cpp

import (
	"fmt"
	"os"

	"gitlab.com/king011/go-binary/core/handler/cpp/names"

	"gitlab.com/king011/go-binary/core/types"
)

type _Marshal struct {
	f         *os.File
	t         *types.Type
	className string
}

func (m *_Marshal) Write() (e error) {
	e = m.marshalSize()
	if e != nil {
		return
	}
	e = m.marshal()
	if e != nil {
		return
	}
	e = m.unmarshal()
	if e != nil {
		return
	}
	return
}
func (m *_Marshal) marshalFieldUser(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if symbol.IsRepeat() {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (!%s.empty())
	{
		use = binarycpp::marshal_enums(ctx, buffer, capacity, use, %v, %s);
	}
`,
				fieldName, item.ID, fieldName,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`	use = binarycpp::marshal(ctx, buffer, capacity, use, %v, (std::int16_t)(%s));
`,
				item.ID, fieldName,
			))
		}
	} else {
		if symbol.IsRepeat() {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (!%s.empty())
	{
		use = binarycpp::marshal_types(ctx, buffer, capacity, use, %v, %v);
	}
`,
				fieldName,
				item.ID, fieldName,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (%v)
	{
		use = binarycpp::marshal_type(ctx, buffer, capacity, use, %v, %v.get());
	}
`,
				fieldName,
				item.ID, fieldName,
			))
		}
	}
	return
}
func (m *_Marshal) marshalField(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		_, e = m.f.WriteString(fmt.Sprintf(`	use = binarycpp::marshal(ctx, buffer, capacity, use, %v, %s);
`,
			item.ID, names.FieldName(item.Name),
		))
	} else {
		e = m.marshalFieldUser(item)
	}
	return
}
func (m *_Marshal) marshal() (e error) {
	_, e = m.f.WriteString(fmt.Sprintf(`int %s_t::marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use) const
{
`,
		m.className,
	))
	if e != nil {
		return
	}

	items := m.t.Fields().Items()
	for _, item := range items {
		e = m.marshalField(item)
		if e != nil {
			return
		}
	}

	_, e = m.f.WriteString(`	return use;
}
`)
	if e != nil {
		return
	}
	return
}
