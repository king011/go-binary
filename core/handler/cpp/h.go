package cpp

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/handler/cpp/names"

	"gitlab.com/king011/go-binary/core/types"
)

func (c _Codes) writeH(dst string, pkg *types.Package, ts []*types.Type) (e error) {
	f, e := c.create(dst + ".h")
	if e != nil {
		return
	}
	defer f.Close()

	_, e = f.WriteString(
		fmt.Sprintf(`#pragma once
#include <binary_cpp.h>
`,
		))
	if e != nil {
		return
	}

	// include
	e = c.writeHIncludes(f, pkg, ts)
	if e != nil {
		return
	}
	namespace := names.Namespace(pkg.Name())
	_, e = f.WriteString(
		fmt.Sprintf(`namespace %s
{
`,
			namespace,
		))
	if e != nil {
		return
	}

	// 枚舉
	e = c.writeHEnums(f, ts)
	if e != nil {
		return
	}

	// class
	c.writeHTypes(f, ts)
	if e != nil {
		return
	}

	_, e = f.WriteString(
		fmt.Sprintf(`}; // namespace %s
`,
			namespace,
		))
	if e != nil {
		return
	}
	return
}
func (c _Codes) writeHIncludes(f *os.File, pkg *types.Package, ts []*types.Type) (e error) {
	count := len(ts)
	if count == 0 {
		return
	}

	keys := make(map[string]bool)
	for i := 0; i < count; i++ {
		t := ts[i]
		fields := t.Fields()
		if fields != nil {
			for _, item := range fields.Items() {
				_, e = c.writeHInclude(f, pkg, keys, item)
				if e != nil {
					return
				}
			}
		}
	}
	return
}
func (c _Codes) writeHInclude(f *os.File, pkg *types.Package, keys map[string]bool, field *types.Field) (ok bool, e error) {
	if field.Symbol.IsCore() {
		return
	}
	t, e := c.ctx.Find(field.Symbol.Package(), field.Symbol.Name())
	if e != nil {
		return
	}
	if t.Package().Name() == pkg.Name() {
		return
	}
	key := names.Include(t.Package().Name())
	if keys[key] {
		return
	}
	keys[key] = true
	_, e = f.WriteString(fmt.Sprintf("#include <%s>\n", key))
	if e != nil {
		return
	}
	ok = true
	return
}
func (c _Codes) writeHEnums(f *os.File, ts []*types.Type) (e error) {
	count := len(ts)
	if count == 0 {
		return
	}
	for i := 0; i < len(ts); i++ {
		t := ts[i]
		enum := t.Enum()
		if enum != nil {
			className := names.ClassName(t.Name())
			_, e = f.WriteString(fmt.Sprintf(`enum %s_t
{
`,
				className,
			))
			if e != nil {
				return
			}

			items := enum.Items()
			for _, item := range items {
				f.WriteString(fmt.Sprintf(`	%v = %v,
`,
					names.Filename(item.Name), item.Val,
				))
				if e != nil {
					return
				}
			}

			_, e = f.WriteString("};\n")
			if e != nil {
				return
			}

			_, e = f.WriteString(fmt.Sprintf(`const char *enum_%s_string(%s_t val);
%s_t enum_%s_val(const char *str);

`,
				className, className,
				className, className,
			))

			return
		}
	}
	return
}
func (c _Codes) writeHTypes(f *os.File, ts []*types.Type) (e error) {
	count := len(ts)
	if count == 0 {
		return
	}
	for i := 0; i < len(ts); i++ {
		if ts[i].Fields() == nil {
			continue
		}
		className := names.ClassName(ts[i].Name())
		_, e = f.WriteString(fmt.Sprintf(`class %s_t;
`,
			className,
		))
		if e != nil {
			return
		}
	}

	for i := 0; i < len(ts); i++ {
		if ts[i].Fields() == nil {
			continue
		}
		cs := &_Class{
			f: f,
			t: ts[i],
		}
		e = cs.Write()
		if e != nil {
			return
		}
	}
	return
}
func (c _Codes) writeCC(dst string, pkg *types.Package, ts []*types.Type) (e error) {
	f, e := c.create(dst + ".cpp")
	if e != nil {
		return
	}
	defer f.Close()
	namespace := names.Namespace(pkg.Name())
	_, e = f.WriteString(
		fmt.Sprintf(`#include "%s.h"
namespace %s
{
`,
			filepath.Base(dst),
			namespace,
		))
	if e != nil {
		return
	}
	// 枚舉
	for i := 0; i < len(ts); i++ {
		e = c.writeCEnums(f, ts[i])
		if e != nil {
			return
		}
	}
	// class
	for i := 0; i < len(ts); i++ {
		t := ts[i]
		if t.Fields() == nil {
			continue
		}
		c := _Class{
			Writer: handler.Writer{
				W: f,
			},
			f: f,
			t: t,
		}
		e = c.writeC()
		if e != nil {
			return
		}
	}

	_, e = f.WriteString(
		fmt.Sprintf(`}; // namespace %s
`,
			namespace,
		))
	if e != nil {
		return
	}
	return
}
