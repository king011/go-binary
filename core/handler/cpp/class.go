package cpp

import (
	"fmt"
	"os"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/handler/cpp/names"
	"gitlab.com/king011/go-binary/core/types"
)

type _Class struct {
	handler.Writer
	f *os.File
	t *types.Type
}

func (c *_Class) Write() (e error) {
	e = c.writeH()
	if e != nil {
		return
	}

	return
}
func (c *_Class) writeC() (e error) {
	className := names.ClassName(c.t.Name())
	_, e = c.f.WriteString(fmt.Sprintf(`%s_t::%s_t()
{
	reset();
}
%s_t::~%s_t()
{
}
`,
		className, className,
		className, className,
	))
	if e != nil {
		return
	}

	b := _Base{
		Writer: handler.Writer{
			W: c.W,
		},
		f:         c.f,
		t:         c.t,
		className: className,
	}
	e = b.Write()
	if e != nil {
		return
	}

	m := &_Marshal{
		f:         c.f,
		t:         c.t,
		className: className,
	}
	e = m.Write()
	if e != nil {
		return
	}
	return
}

func (c *_Class) writeH() (e error) {
	className := names.ClassName(c.t.Name())
	_, e = c.f.WriteString(fmt.Sprintf(`class %s_t : public binarycpp::base_t
{
public:
	%s_t();
	virtual ~%s_t();
	virtual int marshal_size(const binarycpp::context_t &ctx, int use = 0) const;
	virtual int marshal(const binarycpp::context_t &ctx, std::uint8_t *buffer, int capacity, int use = 0) const;
	virtual void unmarshal(const binarycpp::context_t &ctx, const std::uint8_t *input, std::size_t n);
	virtual void reset();
	void swap(%s_t &other);
	void copy_from(const %s_t &other);
	void copy_from(const %s_t *other);
	void copy_from(const BINARY_CPP_SHARED_PTR(%s_t) & other);
	BINARY_CPP_SHARED_PTR(%s_t)
	clone() const;
`,
		className,
		className, className,
		className,
		className, className, className,
		className,
	))
	if e != nil {
		return
	}

	// Fields
	items := c.t.Fields().Items()
	for _, item := range items {
		e = c.writeHField(item)
		if e != nil {
			return
		}
	}

	_, e = c.f.WriteString(`};

`)
	if e != nil {
		return
	}
	return
}
func (c *_Class) writeHField(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = c.writeHFieldCore(item)
	} else {
		e = c.writeHFieldUser(item)
	}
	return
}
func (c *_Class) writeHFieldCore(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	valName := names.ValName(symbol)
	if symbol.IsRepeat() {
		_, e = c.f.WriteString(fmt.Sprintf(`	std::vector<%s> %s;
`,
			valName, fieldName,
		))
	} else {
		_, e = c.f.WriteString(fmt.Sprintf(`	%s %s;
`,
			valName, fieldName,
		))
	}
	return
}
func (c *_Class) writeHFieldUser(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	valName := names.ValName(symbol)
	if symbol.IsEnum() {
		if symbol.IsRepeat() {
			_, e = c.f.WriteString(fmt.Sprintf(`	std::vector<%s> %s;
`,
				valName, fieldName,
			))
		} else {
			_, e = c.f.WriteString(fmt.Sprintf(`	%s %s;
`,
				valName, fieldName,
			))
		}
	} else {
		if symbol.IsRepeat() {
			_, e = c.f.WriteString(fmt.Sprintf(`	std::vector<BINARY_CPP_SHARED_PTR(%s)> %s;
`,
				valName, fieldName,
			))
		} else {
			_, e = c.f.WriteString(fmt.Sprintf(`	BINARY_CPP_SHARED_PTR(%s) %s;
`,
				valName, fieldName,
			))
		}
	}

	return
}
