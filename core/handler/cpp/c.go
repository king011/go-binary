package cpp

import (
	"fmt"
	"os"

	"gitlab.com/king011/go-binary/core/handler/cpp/names"
	"gitlab.com/king011/go-binary/core/types"
)

func (c _Codes) writeCEnums(f *os.File, t *types.Type) (e error) {
	enum := t.Enum()
	if enum == nil {
		return
	}
	className := names.ClassName(t.Name())
	// to string
	_, e = f.WriteString(fmt.Sprintf(`const char *enum_%s_string(%s_t val)
{
`,
		className, className,
	))
	if e != nil {
		return
	}

	_, e = f.WriteString(`	const char *str;
	switch (val)
	{
`)
	if e != nil {
		return
	}

	items := enum.Items()
	for _, item := range items {
		f.WriteString(fmt.Sprintf(`	case %v:
		str = "%s";
		break;
`,
			item.Val,
			item.NameString(),
		))
	}

	_, e = f.WriteString(`	default:
		str = "unknow";
		break;
	}
	return str;
}
`)
	if e != nil {
		return
	}

	// to val
	_, e = f.WriteString(fmt.Sprintf(`%s_t enum_%s_val(const char *str)
{
`,
		className, className,
	))
	if e != nil {
		return
	}

	_, e = f.WriteString(`	uint16_t val = 0;
`)
	if e != nil {
		return
	}

	for i, item := range items {
		var str string
		if i == 0 {
			str = "if"
		} else {
			str = "else if"
		}
		_, e = f.WriteString(fmt.Sprintf(`	%s (!strcmp(str, "%s"))
	{
		val = %v;
	}
`,
			str, item.NameString(),
			item.Val,
		))
		if e != nil {
			return
		}
	}

	_, e = f.WriteString(fmt.Sprintf(`	
	return (%s_t)(val);
}
`,
		className,
	))
	if e != nil {
		return
	}
	return
}
