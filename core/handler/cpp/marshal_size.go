package cpp

import (
	"fmt"

	"gitlab.com/king011/go-binary/core/handler/cpp/names"

	"gitlab.com/king011/go-binary/core/types"
)

func (m *_Marshal) marshalSizeFieldUser(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsEnum() {
		if symbol.IsRepeat() {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (!%s.empty())
	{
        use = binarycpp::marshal_size_add(ctx, use, 4 + %s.size() * 2);
	}
`,
				fieldName, fieldName,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (%s)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
	}
`,
				fieldName,
			))
		}
	} else {
		if symbol.IsRepeat() {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (!%v.empty())
	{
		use = binarycpp::marshal_types_size(ctx, use, %v);
	}
`,
				fieldName,
				fieldName,
			))
		} else {
			_, e = m.f.WriteString(fmt.Sprintf(`	if (%v)
	{
		use = binarycpp::marshal_size_add(ctx, use, 2 + 2);
		use = %v->marshal_size(ctx, use);
	}
`,
				fieldName, fieldName,
			))
		}
	}
	return
}

func (m *_Marshal) marshalSizeField(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		_, e = m.f.WriteString(fmt.Sprintf(`	use = binarycpp::marshal_size(ctx, use, %s);
`,
			names.FieldName(item.Name),
		))
	} else {
		e = m.marshalSizeFieldUser(item)
	}
	return
}
func (m *_Marshal) marshalSize() (e error) {
	_, e = m.f.WriteString(fmt.Sprintf(`int %s_t::marshal_size(const binarycpp::context_t &ctx, int use) const
{
`,
		m.className,
	))
	if e != nil {
		return
	}

	items := m.t.Fields().Items()
	for _, item := range items {
		e = m.marshalSizeField(item)
		if e != nil {
			return
		}
	}
	_, e = m.f.WriteString(`	return use;
}
`)
	if e != nil {
		return
	}
	return
}
