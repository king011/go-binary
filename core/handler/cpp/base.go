package cpp

import (
	"fmt"
	"os"

	"gitlab.com/king011/go-binary/core/handler"
	"gitlab.com/king011/go-binary/core/handler/cpp/names"

	"gitlab.com/king011/go-binary/core/types"
)

type _Base struct {
	handler.Writer
	f         *os.File
	t         *types.Type
	className string
}

func (b *_Base) Write() (e error) {
	e = b.reset()
	if e != nil {
		return
	}
	e = b.swap()
	if e != nil {
		return
	}

	e = b.copy()
	if e != nil {
		return
	}
	e = b.clone()
	if e != nil {
		return
	}

	return
}
func (b *_Base) copyItemUser(item *types.Field, fieldName string) (e error) {
	symbol := item.Symbol
	if symbol.IsRepeat() {
		_, e = b.f.WriteString(fmt.Sprintf(`		binarycpp::copy_from_types(%v, other.%v);
`,
			fieldName, fieldName,
		))
	} else {
		_, e = b.f.WriteString(fmt.Sprintf(`		if (other.%s)
		{
			%s = other.%s->clone();
		}
		else
		{
			%s.reset();
		}
`,
			fieldName,
			fieldName, fieldName,
			fieldName,
		))
	}
	return
}
func (b *_Base) copyItem(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsClass() {
		e = b.copyItemUser(item, fieldName)
	} else {
		_, e = b.f.WriteString(fmt.Sprintf(`		%s = other.%s;
`,
			fieldName, fieldName,
		))
	}
	return
}
func (b *_Base) clone() (e error) {
	className := b.className
	_, e = b.f.WriteString(fmt.Sprintf(`BINARY_CPP_SHARED_PTR(%v_t)
%v_t::clone() const
{
	BINARY_CPP_SHARED_PTR(%v_t)
	p;
	try
	{
		p = BINARY_CPP_MAKE_SHARED(%v_t);
		p->copy_from(*this);
	}
	BINARY_CPP_CATCH
	return p;
}
`,
		className, className,
		className, className,
	))
	if e != nil {
		return
	}
	return
}
func (b *_Base) copy() (e error) {
	className := b.className
	_, e = b.f.WriteString(fmt.Sprintf(`void %s_t::copy_from(const %s_t &other)
{
	try
	{
`,
		className, className,
	))
	if e != nil {
		return
	}
	items := b.t.Fields().Items()
	for _, item := range items {
		e = b.copyItem(item)
		if e != nil {
			return
		}
	}
	e = b.WriteStringf(`	}
	BINARY_CPP_CATCH_RESET
}
void %v_t::copy_from(const %v_t *other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
void %v_t::copy_from(const BINARY_CPP_SHARED_PTR(%v_t) & other)
{
	if(other)
	{
		copy_from(*other);
	}
	else
	{
		reset();
	}
}
`,
		className, className,
		className, className,
	)
	if e != nil {
		return
	}
	return
}
func (b *_Base) swapItem(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsRepeat() || symbol.IsClass() {
		_, e = b.f.WriteString(fmt.Sprintf(`	%s.swap(other.%s);
`,
			fieldName, fieldName,
		))
	} else {
		_, e = b.f.WriteString(fmt.Sprintf(`	binarycpp::swap(%s, other.%s);
`,
			fieldName, fieldName,
		))
	}
	return
}
func (b *_Base) swap() (e error) {
	className := b.className
	_, e = b.f.WriteString(fmt.Sprintf(`void %s_t::swap(%s_t &other)
{
`,
		className, className,
	))
	if e != nil {
		return
	}

	items := b.t.Fields().Items()
	for _, item := range items {
		e = b.swapItem(item)
		if e != nil {
			return
		}
	}

	_, e = b.f.WriteString("}\n")
	if e != nil {
		return
	}
	return
}
func (b *_Base) freeItemUser(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)

	if symbol.IsRepeat() {
		_, e = b.f.WriteString(fmt.Sprintf(`	for (std::size_t i = 0; i < %s.size(); i++)
	{
		if (%s[i])
		{
			BINARY_CPP_DELETE(%s[i]);
		}
	}
`,
			fieldName, fieldName, fieldName,
		))
	} else {
		_, e = b.f.WriteString(fmt.Sprintf(`	if (%s)
	{
		BINARY_CPP_DELETE(%s);
	}
`,
			fieldName, fieldName,
		))
	}

	return
}
func (b *_Base) freeItem(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsClass() {
		e = b.freeItemUser(item)
	}
	return
}

func (b *_Base) resetItemUser(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsRepeat() {
		_, e = b.f.WriteString(fmt.Sprintf(`	%s.clear();
`,
			fieldName,
		))
	} else {
		if symbol.IsEnum() {
			className := names.ValName(symbol)
			_, e = b.f.WriteString(fmt.Sprintf(`	%s = (%s)(0);
`,
				fieldName, className,
			))
		} else {
			_, e = b.f.WriteString(fmt.Sprintf(`	%s.reset();
`,
				fieldName,
			))
		}
	}

	return
}
func (b *_Base) resetItemCore(item *types.Field) (e error) {
	symbol := item.Symbol
	fieldName := names.FieldName(item.Name)
	if symbol.IsRepeat() {
		_, e = b.f.WriteString(fmt.Sprintf(`	%s.clear();
`,
			fieldName,
		))
	} else {
		switch symbol.Name() {
		case "string":
			_, e = b.f.WriteString(fmt.Sprintf(`	%s.clear();
`,
				fieldName,
			))
		case "bool":
			_, e = b.f.WriteString(fmt.Sprintf(`	%s = false;
`,
				fieldName,
			))
		default:
			_, e = b.f.WriteString(fmt.Sprintf(`	%s = 0;
`,
				fieldName,
			))
		}
	}
	return
}
func (b *_Base) resetItem(item *types.Field) (e error) {
	symbol := item.Symbol
	if symbol.IsCore() {
		e = b.resetItemCore(item)
	} else {
		e = b.resetItemUser(item)
	}
	return
}
func (b *_Base) reset() (e error) {
	className := b.className
	_, e = b.f.WriteString(fmt.Sprintf(`void %s_t::reset()
{
`,
		className,
	))
	if e != nil {
		return
	}

	items := b.t.Fields().Items()
	for _, item := range items {
		e = b.resetItem(item)
		if e != nil {
			return
		}
	}

	_, e = b.f.WriteString("}\n")
	if e != nil {
		return
	}
	return
}
