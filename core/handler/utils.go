package handler

import (
	"fmt"
	"io"
)

// Writer 提供方便的 寫入功能
type Writer struct {
	W io.StringWriter
}

// WriteString .
func (w Writer) WriteString(a ...interface{}) (e error) {
	_, e = w.W.WriteString(fmt.Sprint(a...))
	return
}

// WriteStringln .
func (w Writer) WriteStringln(a ...interface{}) (e error) {
	_, e = w.W.WriteString(fmt.Sprintln(a...))
	return
}

// WriteStringf .
func (w Writer) WriteStringf(format string, a ...interface{}) (e error) {
	_, e = w.W.WriteString(fmt.Sprintf(format, a...))
	return
}
