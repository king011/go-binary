package core

import (
	"errors"
	"fmt"

	"gitlab.com/king011/go-binary/core/types"
	duk "gopkg.in/olebedev/go-duktape.v3"
)

func (c *Core) nativeRegisterType(scripts *duk.Context) int {
	top := scripts.GetTop()
	if top < 1 || !scripts.IsString(0) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter pkg")
		return 1
	}
	if top < 2 || !scripts.IsString(1) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter typename")
		return 1
	}
	if top < 3 || !scripts.IsString(2) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "unknow __filename")
		return 1
	}

	pkg := scripts.ToString(0)
	name := scripts.ToString(1)
	filename := scripts.ToString(2)
	var e error
	if top > 3 && scripts.IsBoolean(3) && scripts.ToBoolean(3) {
		e = c.context.Register(pkg, name, filename, true)
	} else {
		e = c.context.Register(pkg, name, filename, false)
	}
	if e != nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
		return 1
	}
	return 0
}
func (c *Core) nativeRegisterEnum(scripts *duk.Context) int {
	top := scripts.GetTop()
	if top < 1 || !scripts.IsString(0) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter pkg")
		return 1
	}
	if top < 2 || !scripts.IsString(1) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter typename")
		return 1
	}
	if top < 3 || !scripts.IsString(2) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "unknow field")
		return 1
	}
	pkg := scripts.ToString(0)
	name := scripts.ToString(1)
	t, e := c.context.Find(pkg, name)
	if e != nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
		return 1
	}
	enum := t.Enum()
	if enum == nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, "not a enum [%s %s]", pkg, name)
		return 1
	}
	field := scripts.ToString(2)

	if top > 3 && scripts.IsNumber(3) {
		e = enum.RegisterValue(field, int16(scripts.ToInt(3)))
	} else {
		e = enum.Register(field)
	}
	if e != nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
		return 1
	}
	return 0
}
func (c *Core) nativeRegisterEnums(scripts *duk.Context) int {
	top := scripts.GetTop()
	if top < 1 || !scripts.IsString(0) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter pkg")
		return 1
	}
	if top < 2 || !scripts.IsString(1) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter typename")
		return 1
	}
	if top < 3 || !scripts.IsObject(2) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "unknow field")
		return 1
	}
	pkg := scripts.ToString(0)
	name := scripts.ToString(1)
	t, e := c.context.Find(pkg, name)
	if e != nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
		return 1
	}
	enum := t.Enum()
	if enum == nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, "not a enum [%s %s]", pkg, name)
		return 1
	}
	scripts.Enum(2, duk.EnumOwnPropertiesOnly)
	for scripts.Next(-1, true) {
		if !scripts.IsString(-1) {
			scripts.PushErrorObjectVa(duk.ErrRetError, "field only support string")
			return 1
		}
		field := scripts.ToString(-1)
		e = enum.Register(field)
		if e != nil {
			scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
			return 1
		}
		scripts.Pop2()
	}
	scripts.Pop()
	return 0
}
func (c *Core) nativeRegisterField(scripts *duk.Context) int {
	top := scripts.GetTop()
	if top < 1 || !scripts.IsString(0) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter pkg")
		return 1
	}
	if top < 2 || !scripts.IsString(1) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "invalid parameter typename")
		return 1
	}
	if top < 3 || !scripts.IsNumber(2) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "unknow field id")
		return 1
	}
	if top < 4 || !scripts.IsString(3) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "unknow field name")
		return 1
	}
	if top < 5 || !scripts.IsObject(4) {
		scripts.PushErrorObjectVa(duk.ErrRetError, "unknow field symbol")
		return 1
	}
	pkg := scripts.ToString(0)
	name := scripts.ToString(1)
	t, e := c.context.Find(pkg, name)
	if e != nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
		return 1
	}
	fields := t.Fields()
	if fields == nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, "is a enum [%s %s]", pkg, name)
		return 1
	}

	id := scripts.ToUint16(2)
	name = scripts.ToString(3)
	symbol, e := c.registerFieldSymbol(scripts)
	if e != nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
		return 1
	}
	e = fields.Register(id, name, symbol)
	if e != nil {
		scripts.PushErrorObjectVa(duk.ErrRetError, e.Error())
		return 1
	}
	return 0
}
func (c *Core) registerFieldSymbol(scripts *duk.Context) (symbol types.Symbol, e error) {
	scripts.GetPropString(-1, "pkg")
	if !scripts.IsString(-1) {
		e = errors.New("unknow symbol.pkg")
		return
	}
	pkg := scripts.ToString(-1)
	scripts.Pop()

	scripts.GetPropString(-1, "name")
	if !scripts.IsString(-1) {
		e = errors.New("unknow symbol.name")
		return
	}
	name := scripts.ToString(-1)
	scripts.Pop()

	scripts.GetPropString(-1, "repeat")
	if !scripts.IsBoolean(-1) {
		e = errors.New("unknow symbol.repeat")
		return
	}
	repeat := scripts.ToBoolean(-1)
	scripts.Pop()

	scripts.GetPropString(-1, "write")
	if !scripts.IsNumber(-1) {
		e = errors.New("unknow symbol.write")
		return
	}
	write := scripts.ToUint(-1)
	scripts.Pop()
	if write < types.Write8 && write > types.WriteLength {
		e = fmt.Errorf("not support symbol.write [%v]", write)
		return
	}

	scripts.GetPropString(-1, "core")
	if !scripts.IsNumber(-1) {
		e = errors.New("unknow symbol.core")
		return
	}
	core := scripts.ToInt(-1)
	scripts.Pop()

	switch core {
	case types.SymbolCore:
		symbol = types.NewCoreSymbol(pkg, name, repeat, uint8(write))
	case types.SymbolUser:
		symbol = types.NewUserSymbol(pkg, name, repeat, uint8(write))
	default:
		e = fmt.Errorf("not support core : %v %v %v %v", pkg, name, repeat, core)
	}
	return
}
func (c *Core) native() (e error) {
	scripts := c.scripts
	scripts.PushObject()
	{
		scripts.PushGoFunction(c.nativeRegisterType)
		scripts.PutPropString(-2, "Register")
		scripts.PushGoFunction(c.nativeRegisterEnum)
		scripts.PutPropString(-2, "RegisterEnum")
		scripts.PushGoFunction(c.nativeRegisterEnums)
		scripts.PutPropString(-2, "RegisterEnums")
		scripts.PushGoFunction(c.nativeRegisterField)
		scripts.PutPropString(-2, "RegisterField")
	}
	scripts.PutGlobalString("__core__")
	e = c.nativeEval()
	return
}
func (c *Core) nativeEval() (e error) {
	scripts := c.scripts
	e = scripts.PevalString(`
(function(global){
	"use strict";
	// Write8 byte bool
	const Write8 = 1;
	// Write16 int16 uint16
	const Write16 = 2;
	// Write32 float32 int32 uint32
	const Write32 = 3;
	// Write64 float64 int64 uint64
	const Write64 = 4;
	// WriteLength bytes strings type repeat
	const WriteLength = 5;
	// WriteBits bit repeat
	const WriteBits = 6;
	function UseSymbol(pkg,name,repeat,core,write){
		if(repeat==undefined){
			repeat=false;
		}
		return {
			pkg:pkg,
			name:name,
			repeat:repeat,
			core:core,
			write:write,
		}
	};
	function NewEnum(pkg,name){
		return {
			register(id,val){
				var e;
				if(arguments.length>2){
					e=__core__.RegisterEnums(pkg,name,arguments);
				}else{
					e=__core__.RegisterEnum(pkg,name,id,val);
				}
				if(e){
					throw e;
				}
				return this;
			},
		};
	};
	function NewType(pkg,name){
		return {
			register(obj){
				if(arguments.length==0){
					return this;
				}
				var e;
				if(arguments.length==3 && typeof arguments[0] != "object"){
					e=__core__.RegisterField(pkg,name,arguments[0],arguments[1],arguments[2]);
					if(e){
						throw e;
					}
				}else if(arguments.length==1){
					if(obj.length){
						//array
						for(var i=0;i<obj.length;i++){
							e=__core__.RegisterField(pkg,name,obj[i].id,obj[i].name,obj[i].symbol);
							if(e){
								throw e;
							}
						}
					}else{
						//one
						e=__core__.RegisterField(pkg,name,obj.id,obj.name,obj.symbol);
						if(e){
							throw e;
						}
					}
				}else{
					//...
					for(var i=0;i<arguments.length;i++){
						e=__core__.RegisterField(pkg,name,arguments[i].id,arguments[i].name,arguments[i].symbol);
						if(e){
							throw e;
						}
					}
				}
				return this;
			},
		};
	};
	global.core = {
		RegisterType(pkg,name){
			const e=__core__.Register(pkg,name,__filename,false);
			if(e){
				throw e;
			}
			return NewType(pkg,name);
		},
		RegisterEnum(pkg,name){
			const e=__core__.Register(pkg,name,__filename,true);
			if(e){
				throw e;
			}
			return NewEnum(pkg,name);
		},
		UseSymbol(pkg,name,repeat){
			return UseSymbol(pkg,name,repeat,0,WriteLength)
		},
		"int16":UseSymbol("core", "int16", false,1,Write16),
		"int32":UseSymbol("core", "int32", false,1,Write32),
		"int64":UseSymbol("core", "int64", false,1,Write64),
		"uint16":UseSymbol("core", "uint16", false,1,Write16),
		"uint32":UseSymbol("core", "uint32", false,1,Write32),
		"uint64":UseSymbol("core", "uint64", false,1,Write64),
		"float32":UseSymbol("core", "float32", false,1,Write32),
		"float64":UseSymbol("core", "float64", false,1,Write64),
		"byte":UseSymbol("core", "byte", false,1,Write8),
		"bool":UseSymbol("core", "bool", false,1,Write8),
		"string":UseSymbol("core", "string", false,1,WriteLength),
		"int16s": UseSymbol("core", "int16", true,1,WriteLength),
		"int32s": UseSymbol("core", "int32", true,1,WriteLength),
		"int64s": UseSymbol("core", "int64", true,1,WriteLength),
		"uint16s":UseSymbol("core", "uint16", true,1,WriteLength),
		"uint32s":UseSymbol("core", "uint32", true,1,WriteLength),
		"uint64s":UseSymbol("core", "uint64", true,1,WriteLength),
		"float32s":UseSymbol("core", "float32", true,1,WriteLength),
		"float64s":UseSymbol("core", "float64", true,1,WriteLength),
		"bytes":UseSymbol("core", "byte", true,1,WriteLength),
		"bools":UseSymbol("core", "bool", true,1,WriteBits),
		"strings":UseSymbol("core", "string", true,1,WriteLength),
	};
})(this)
`)
	scripts.Pop()
	return
}
