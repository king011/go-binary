package types

import (
	"fmt"
	"sort"
)

// Context 上下文
type Context struct {
	//  註冊的 包
	keys map[string]*Package
}

// NewContext 創建 上下文
func NewContext() *Context {
	return &Context{
		keys: make(map[string]*Package),
	}
}

// Register 註冊一個型別
func (c *Context) Register(pkg, name, filename string, enum bool) (e error) {
	pkg, e = ToPackageName(pkg)
	if e != nil {
		return
	}
	name, e = ToFieldName(name)
	if e != nil {
		return
	}

	// 查找 包
	p, ok := c.keys[pkg]
	if !ok {
		p = NewPackage(pkg)
		c.keys[pkg] = p
	}
	p.Register(name, filename, enum)
	return
}

// Keys 返回 所有 包
func (c *Context) Keys() map[string]*Package {
	return c.keys
}

// Items 返回 所有 包
func (c *Context) Items() []*Package {
	items := make([]*Package, 0, len(c.keys))
	for _, v := range c.keys {
		items = append(items, v)
	}
	sort.Sort(sortPackage(items))
	return items
}

type sortPackage []*Package

// Len is the number of elements in the collection.
func (arrs sortPackage) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs sortPackage) Less(i, j int) bool {
	return arrs[i].name < arrs[j].name
}

// Swap swaps the elements with indexes i and j.
func (arrs sortPackage) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}

// Find 返回 型別
func (c *Context) Find(pkg, name string) (t *Type, e error) {
	p, ok := c.keys[pkg]
	if !ok {
		e = fmt.Errorf("package not found [%s]", pkg)
		return
	}
	t, ok = p.keys[name]
	if !ok {
		e = fmt.Errorf("type not found [%s %s]", pkg, name)
		return
	}
	return
}
