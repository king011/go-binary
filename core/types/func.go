package types

import (
	"fmt"
	"regexp"
	"strings"
)

var matchPackage = regexp.MustCompile(`^[a-z][a-z0-9\/]*$`)
var matchField = regexp.MustCompile(`^[a-z][a-z0-9\_]*$`)

// ToPackageName 驗證包名 並返回 標準名稱
func ToPackageName(pkg string) (name string, e error) {
	name = strings.ToLower(strings.TrimSpace(pkg))
	if strings.HasSuffix(name, "/") {
		e = fmt.Errorf(`package name not support end with '/' [%s]`, pkg)
		return
	}
	if strings.Index(name, "//") != -1 {
		e = fmt.Errorf(`package name not support '//' [%s]`, pkg)
		return
	}
	if !matchPackage.MatchString(name) {
		e = fmt.Errorf(`package name only support 'a-z' '0-9' '/' [%s]`, pkg)
		return
	}
	strs := strings.Split(name, "/")
	for _, str := range strs {
		if str[0] < 'a' || str[0] > 'z' {
			e = fmt.Errorf(`package name only support start with 'a-z' [%s]`, pkg)
			return
		}
	}
	return
}

// ToFieldName 驗證名稱 並返回 標準名稱
func ToFieldName(typeName string) (name string, e error) {
	name = strings.ToLower(strings.TrimSpace(typeName))
	if strings.HasSuffix(name, "_") {
		e = fmt.Errorf(`field name not support end with '_' [%s]`, typeName)
		return
	}
	if strings.Index(name, "__") != -1 {
		e = fmt.Errorf(`field name not support '__' [%s]`, typeName)
		return
	}
	if !matchField.MatchString(name) {
		e = fmt.Errorf(`field name only support 'a-z' '0-9' '_' [%s]`, typeName)
		return
	}
	strs := strings.Split(name, "_")
	for _, str := range strs {
		if str[0] < 'a' || str[0] > 'z' {
			e = fmt.Errorf(`field name only support start with 'a-z' [%s]`, typeName)
			return
		}
	}
	return
}
