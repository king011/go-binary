package types

// Type .
type Type struct {
	pkg      *Package
	name     string
	filename string
	enum     *Enum
	fields   *Fields
}

// NewType 創建型別
func NewType(pkg *Package, name, filename string, enum bool) *Type {
	var useEnum *Enum
	var fields *Fields
	if enum {
		useEnum = NewEnum(pkg.name, name)
	} else {
		fields = NewFields(pkg.name, name)
	}
	return &Type{
		pkg:      pkg,
		name:     name,
		filename: filename,
		enum:     useEnum,
		fields:   fields,
	}
}

// Name 返回 型別 名稱
func (t *Type) Name() string {
	return t.name
}

// Enum 返回 枚舉
func (t *Type) Enum() *Enum {
	return t.enum
}

// Filename 返回 定義檔案
func (t *Type) Filename() string {
	return t.filename
}

// Fields 返回 屬性
func (t *Type) Fields() *Fields {
	return t.fields
}

// Package 返回 所在包
func (t *Type) Package() *Package {
	return t.pkg
}
