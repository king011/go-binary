package types

import "fmt"

// Error 錯誤定義
type Error struct {
	Filename string
	Message  string
}

func (e Error) Error() string {
	if e.Filename == "" {
		return e.Message
	}
	return fmt.Sprintf("%s [%s]", e.Message, e.Filename)
}

// SetErrorFilename 設置 錯誤
func SetErrorFilename(e error, filename string) error {
	if e0, ok := e.(Error); ok {
		return Error{
			Filename: e0.Filename,
			Message:  e0.Message,
		}
	}
	return Error{
		Filename: filename,
		Message:  e.Error(),
	}
}
