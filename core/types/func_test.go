package types

import "testing"

func TestToPackageName(t *testing.T) {
	reject := []string{
		"",
		"/x12",
		"/x12//z",
		"/x12/",
		"12/x12",
		"x//12",
		"x/y/z12/",
		"x/123/y",
	}
	for _, name := range reject {
		_, e := ToPackageName(name)
		if e == nil {
			t.Fatal("allowed :", name)
		}
	}
	allow := []string{
		"x",
		"x/y",
		"x/y1",
		"x/y1/z",
		"x/y/z",
	}
	for _, name := range allow {
		_, e := ToPackageName(name)
		if e != nil {
			t.Fatal("rejected :", name, e)
		}
	}
}

func TestToFieldName(t *testing.T) {
	reject := []string{
		"",
		"_x12",
		"_x12__z",
		"_x12_",
		"12_x12",
		"x__12",
		"x_y_z12_",
		"x_123_y",
	}
	for _, name := range reject {
		_, e := ToFieldName(name)
		if e == nil {
			t.Fatal("allowed :", name)
		}
	}
	allow := []string{
		"x",
		"x_y",
		"x_y1",
		"x_y1_z",
		"x_y_z",
	}
	for _, name := range allow {
		_, e := ToFieldName(name)
		if e != nil {
			t.Fatal("rejected :", name, e)
		}
	}
}
