package types

import (
	"fmt"
	"sort"
	"strings"
)

// Enum 定義枚舉
type Enum struct {
	pkg string
	t   string
	// 可用值
	id int16
	// 已用值
	keys  map[int16]string
	names map[string]int16
}

// NewEnum 創建枚舉
func NewEnum(pkg, t string) *Enum {

	return &Enum{
		keys:  make(map[int16]string),
		names: make(map[string]int16),
		pkg:   pkg,
		t:     t,
	}
}

// Register 註冊 字段
func (enum *Enum) Register(name string) (e error) {
	name, e = ToFieldName(name)
	if e != nil {
		return
	}
	if _, ok := enum.names[name]; ok {
		e = fmt.Errorf("field already exists [%s]", name)
		return
	}

	val := enum.id
	first := true
	for {
		if val == enum.id {
			if first {
				first = false
			} else {
				break
			}
		}
		if _, ok := enum.keys[val]; !ok {
			enum.register(name, val)
			return
		}
	}
	e = fmt.Errorf("no have idle val")
	return
}
func (enum *Enum) register(name string, val int16) {
	enum.keys[val] = name
	enum.names[name] = val
	if enum.id <= val {
		enum.id = val + 1
	}
}

// RegisterValue 註冊 字段
func (enum *Enum) RegisterValue(name string, val int16) (e error) {
	name, e = ToFieldName(name)
	if e != nil {
		return
	}
	if _, ok := enum.names[name]; ok {
		e = fmt.Errorf("[%v.%v] field already exists [%s]", enum.pkg, enum.t, name)
		return
	}
	if str, ok := enum.keys[val]; ok {
		e = fmt.Errorf("[%v.%v] val already exists [%v=%v]", enum.pkg, enum.t, str, val)
		return
	}
	enum.register(name, val)
	return
}

// Items 返回 枚舉
func (enum *Enum) Items() (items []EnumSymbol) {
	items = make([]EnumSymbol, 0, len(enum.keys))
	for val, name := range enum.keys {
		items = append(items, EnumSymbol{
			Name: name,
			Val:  val,
		})
	}
	sort.Sort(sortEnumSymbol(items))
	return items
}

// EnumSymbol 枚舉 符號信息
type EnumSymbol struct {
	Name string
	Val  int16
}

// NameString 返回名稱 字符串
func (symbol EnumSymbol) NameString() string {
	return strings.ReplaceAll(strings.ToLower(symbol.Name), "_", " ")
}

type sortEnumSymbol []EnumSymbol

// Len is the number of elements in the collection.
func (arrs sortEnumSymbol) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs sortEnumSymbol) Less(i, j int) bool {
	return arrs[i].Val < arrs[j].Val
}

// Swap swaps the elements with indexes i and j.
func (arrs sortEnumSymbol) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}
