package types

import "sort"

// Package 註冊包
type Package struct {
	name  string
	keys  map[string]*Type
	files map[string][]*Type
}

// NewPackage 創建註冊包
func NewPackage(name string) *Package {
	return &Package{
		name:  name,
		keys:  make(map[string]*Type),
		files: make(map[string][]*Type),
	}
}

// Register 註冊一個 型別
func (p *Package) Register(name, filename string, enum bool) (e error) {
	if _, ok := p.keys[name]; ok {
		return
	}
	t := NewType(p, name, filename, enum)
	p.keys[name] = t
	files, ok := p.files[filename]
	if !ok {
		files = make([]*Type, 0, 10)
	}
	files = append(files, t)
	p.files[filename] = files
	return
}

// Keys 返回所有註冊的型別
func (p *Package) Keys() map[string]*Type {
	return p.keys
}

// FileKeys 返回 同個檔案中 註冊的型別
func (p *Package) FileKeys() map[string][]*Type {
	return p.files
}

// Items 返回所有註冊的型別
func (p *Package) Items() []*Type {
	items := make([]*Type, 0, len(p.keys))
	for _, v := range p.keys {
		items = append(items, v)
	}
	sort.Sort(SortType(items))
	return items
}

// SortType 實現 排序
type SortType []*Type

// Len is the number of elements in the collection.
func (arrs SortType) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs SortType) Less(i, j int) bool {
	return arrs[i].name < arrs[j].name
}

// Swap swaps the elements with indexes i and j.
func (arrs SortType) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}

// Name 返回 包名
func (p *Package) Name() string {
	return p.name
}
