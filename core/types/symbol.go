package types

const (
	// SymbolUser 用戶定義的 型別
	SymbolUser = 0
	// SymbolCore 系統型別
	SymbolCore = 1
)
const (
	// Write8 byte bool
	Write8 = 1
	// Write16 int16 uint16
	Write16 = 2
	// Write32 float32 int32 uint32
	Write32 = 3
	// Write64 float64 int64 uint64
	Write64 = 4
	// WriteLength bytes strings type repeat
	WriteLength = 5
	// WriteBits bit repeat
	WriteBits = 6
)

// Symbol 符號接口
type Symbol interface {
	Package() string
	Name() string
	IsRepeat() bool
	Core() int
	IsCore() bool
	IsString() bool
	IsBool() bool
	IsEnum() bool
	IsClass() bool
	SetType(t *Type)
	GetType() *Type
	// 返回 寫入類型
	WriteType() uint8
	WriteString() string
}
type _Symbol struct {
	pkg       string
	name      string
	repeat    bool
	core      int
	t         *Type
	writeType uint8
}

func (symbol *_Symbol) WriteString() string {
	var str string
	switch symbol.writeType {
	case Write8:
		str = "8bit"
	case Write16:
		str = "16bit"
	case Write32:
		str = "32bit"
	case Write64:
		str = "64bit"
	case WriteLength:
		str = "length"
	case WriteBits:
		str = "bits"
	default:
		str = "unknow"
	}
	return str
}
func (symbol *_Symbol) WriteType() uint8 {
	return symbol.writeType
}
func (symbol *_Symbol) IsEnum() bool {
	if symbol.t == nil {
		return false
	}
	return symbol.t.Enum() != nil
}
func (symbol *_Symbol) IsClass() bool {
	if symbol.t == nil {
		return false
	}
	return symbol.t.Fields() != nil
}
func (symbol *_Symbol) Package() string {
	return symbol.pkg
}
func (symbol *_Symbol) Name() string {
	return symbol.name
}
func (symbol *_Symbol) IsRepeat() bool {
	return symbol.repeat
}
func (symbol *_Symbol) Core() int {
	return symbol.core
}
func (symbol *_Symbol) IsCore() bool {
	return symbol.core == SymbolCore
}
func (symbol *_Symbol) IsString() bool {
	return symbol.IsCore() && symbol.name == "string"
}
func (symbol *_Symbol) IsBool() bool {
	return symbol.IsCore() && symbol.name == "bool"
}
func (symbol *_Symbol) SetType(t *Type) {
	symbol.t = t
	if symbol.IsRepeat() || symbol.IsClass() {
		symbol.writeType = WriteLength
	} else {
		symbol.writeType = Write16
	}
}
func (symbol *_Symbol) GetType() *Type {
	return symbol.t
}

// NewCoreSymbol 創建 符號
func NewCoreSymbol(pkg, name string, repeat bool, writeType uint8) Symbol {
	return &_Symbol{
		pkg:       pkg,
		name:      name,
		repeat:    repeat,
		core:      SymbolCore,
		writeType: writeType,
	}
}

// NewUserSymbol 創建 符號
func NewUserSymbol(pkg, name string, repeat bool, writeType uint8) Symbol {
	return &_Symbol{
		pkg:       pkg,
		name:      name,
		repeat:    repeat,
		core:      SymbolUser,
		writeType: writeType,
	}
}
