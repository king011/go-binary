package types

import (
	"fmt"
	"sort"
)

// MaxFieldID 最大 id
const MaxFieldID = 0x1FFF

// Field 屬性
type Field struct {
	ID     uint16
	Name   string
	Symbol Symbol
}

// Fields 屬性
type Fields struct {
	pkg string
	t   string
	// 已用值
	keys  map[uint16]*Field
	names map[string]*Field
}

// NewFields 創建屬性列表
func NewFields(pkg, t string) *Fields {
	return &Fields{
		pkg:   pkg,
		t:     t,
		keys:  make(map[uint16]*Field),
		names: make(map[string]*Field),
	}
}

// Register 註冊 字段
func (fields *Fields) Register(id uint16, name string, symbol Symbol) (e error) {
	if id > MaxFieldID {
		e = fmt.Errorf("[%v.%v] id not support [%v]", fields.pkg, fields.t, id)
		return
	}
	name, e = ToFieldName(name)
	if e != nil {
		return
	}
	if _, ok := fields.keys[id]; ok {
		e = fmt.Errorf("[%v.%v] id already exists [%v]", fields.pkg, fields.t, id)
		return
	}
	if _, ok := fields.names[name]; ok {
		e = fmt.Errorf("[%v.%v] field already exists [%s]", fields.pkg, fields.t, name)
		return
	}

	field := &Field{
		ID:     id,
		Name:   name,
		Symbol: symbol,
	}
	fields.keys[id] = field
	fields.names[name] = field
	return
}

// Items 返回 枚舉
func (fields *Fields) Items() (items []*Field) {
	items = make([]*Field, 0, len(fields.keys))
	for _, field := range fields.keys {
		items = append(items, field)
	}
	sort.Sort(sortFields(items))
	return items
}

type sortFields []*Field

// Len is the number of elements in the collection.
func (arrs sortFields) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs sortFields) Less(i, j int) bool {
	return arrs[i].ID < arrs[j].ID
}

// Swap swaps the elements with indexes i and j.
func (arrs sortFields) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}
