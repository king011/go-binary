module gitlab.com/king011/go-binary

go 1.12

require (
	github.com/spf13/cobra v0.0.5
	gitlab.com/king011/king-go v0.0.8
	gopkg.in/olebedev/go-duktape.v3 v3.0.0-20190709231704-1e4459ed25ff
)
