// 定義 包
const pkg = "zoo/animal"


// 註冊 cat 型別
core.RegisterType(pkg, "cat")
    .register([
        {
            id: 1,
            name: "name",
            symbol: core.string,
        },
        {
            id: 2,
            name: "dog",
            symbol: core.UseSymbol(pkg, "dog"),
        },
        {
            id: 3,
            name: "cat",
            symbol: core.UseSymbol(pkg, "cat"),
        },
    ])
// 註冊 dog 消息
var v = core.RegisterType(pkg, "dog")
    .register([
        {
            id: 1,
            name: "id",
            symbol: core.int64,
        },
        {
            id: 2,
            name: "eat",
            symbol: core.strings,
        },
        {
            id: 3,
            name: "cat",
            symbol: core.UseSymbol(pkg, "cat"),
        },
    ])
