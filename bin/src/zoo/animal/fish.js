// 定義 包
const pkg = "zoo/animal"


// 註冊 fish 空型別
core.RegisterType(pkg, "fish")


// 註冊 pos 枚舉
core.RegisterEnum(pkg, "pos")
    .register("pool")
    .register("cage", 10)
    .register("step", "stairs", "corridor_p0", "corridor_p1")
