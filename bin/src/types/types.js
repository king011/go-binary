// 定義 包
const pkg = "types"

// import 其它包
const pkgZooAnimal = "zoo/animal"

// 註冊 types 型別
core.RegisterType(pkg, "types")
    .register(1, "int16", core.int16)
    .register([
        {
            id: 2,
            name: "int32",
            symbol: core.int32,
        },
        {
            id: 3,
            name: "int64",
            symbol: core.int64,
        },
    ])
    .register(
        {
            id: 4,
            name: "uint16",
            symbol: core.uint16,
        },
        {
            id: 5,
            name: "uint32",
            symbol: core.uint32,
        },
        {
            id: 6,
            name: "uint64",
            symbol: core.uint64,
        },

        {
            id: 7,
            name: "float32",
            symbol: core.float32,
        },
        {
            id: 8,
            name: "float64",
            symbol: core.float64,
        },

        {
            id: 9,
            name: "b",
            symbol: core.byte,
        },
        {
            id: 10,
            name: "ok",
            symbol: core.bool,
        },
        {
            id: 11,
            name: "str",
            symbol: core.string,
        },
        {
            id: 12,
            name: "cat",
            symbol: core.UseSymbol(pkgZooAnimal, "cat"),
        },
        {
            id: 13,
            name: "dog",
            symbol: core.UseSymbol(pkgZooAnimal, "dog"),
        },
        {
            id: 14,
            name: "pos",
            symbol: core.UseSymbol(pkgZooAnimal, "pos"),
        },

        {
            id: 21,
            name: "int16s",
            symbol: core.int16s,
        },
        {
            id: 22,
            name: "int32s",
            symbol: core.int32s,
        },
        {
            id: 23,
            name: "int64s",
            symbol: core.int64s,
        },

        {
            id: 24,
            name: "uint16s",
            symbol: core.uint16s,
        },
        {
            id: 25,
            name: "uint32s",
            symbol: core.uint32s,
        },
        {
            id: 26,
            name: "uint64s",
            symbol: core.uint64s,
        },

        {
            id: 27,
            name: "float32s",
            symbol: core.float32s,
        },
        {
            id: 28,
            name: "float64s",
            symbol: core.float64s,
        },

        {
            id: 29,
            name: "bs",
            symbol: core.bytes,
        },
        {
            id: 30,
            name: "oks",
            symbol: core.bools,
        },
        {
            id: 31,
            name: "strs",
            symbol: core.strings,
        },
        {
            id: 32,
            name: "cats",
            symbol: core.UseSymbol(pkgZooAnimal, "cat", true),
        },
        {
            id: 33,
            name: "dogs",
            symbol: core.UseSymbol(pkgZooAnimal, "dog", true),
        },
        {
            id: 34,
            name: "poss",
            symbol: core.UseSymbol(pkgZooAnimal, "pos", true),
        }
    )
