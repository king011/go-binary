package cmd

import (
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"
	"gitlab.com/king011/go-binary/core"
	"gitlab.com/king011/go-binary/core/handler/golang"
	"gitlab.com/king011/go-binary/utils"
)

func init() {
	handlerID := golang.ID
	basePath := utils.BasePath()
	var src, root, dst string
	var clean bool
	cmd := &cobra.Command{
		Use:   "golang",
		Short: "build *.js and generate golang code",
		Run: func(cmd *cobra.Command, args []string) {
			c, e := core.New()
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Load(src)
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Build(os.Stdout)
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Handle(handlerID, root, dst, clean)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&src,
		"src", "s",
		path.Join(basePath, "src"),
		"source *.js directory",
	)
	flags.StringVarP(&dst,
		"dst", "d",
		path.Join(basePath, "dst", "go"),
		"build output directory",
	)
	flags.StringVarP(&root,
		"root", "r",
		"",
		"root package name",
	)
	flags.BoolVarP(&clean,
		"clean", "c",
		true,
		"clean output directory",
	)
	rootCmd.AddCommand(cmd)
}
