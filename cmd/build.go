package cmd

import (
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/king011/go-binary/core"
	"gitlab.com/king011/go-binary/utils"
)

func init() {
	basePath := utils.BasePath()
	var src string
	cmd := &cobra.Command{
		Use:   "build",
		Short: "build *.js and display memory tree",
		Run: func(cmd *cobra.Command, args []string) {
			c, e := core.New()
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Load(src)
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Build(os.Stdout)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&src,
		"src", "s",
		filepath.Join(basePath, "src"),
		"source *.js directory",
	)

	rootCmd.AddCommand(cmd)
}
