package cmd

import (
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"
	"gitlab.com/king011/go-binary/core"
	"gitlab.com/king011/go-binary/core/handler/cpp"
	"gitlab.com/king011/go-binary/utils"
)

func init() {
	handlerID := cpp.ID
	basePath := utils.BasePath()
	var src, dst string
	var clean bool
	cmd := &cobra.Command{
		Use:   "cpp",
		Short: "build *.js and generate c++ code",
		Run: func(cmd *cobra.Command, args []string) {
			c, e := core.New()
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Load(src)
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Build(os.Stdout)
			if e != nil {
				log.Fatalln(e)
			}
			e = c.Handle(handlerID, "", dst, clean)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&src,
		"src", "s",
		path.Join(basePath, "src"),
		"source *.js directory",
	)
	flags.StringVarP(&dst,
		"dst", "d",
		path.Join(basePath, "dst", "cc"),
		"build output directory",
	)
	flags.BoolVarP(&clean,
		"clean", "c",
		true,
		"clean output directory",
	)
	rootCmd.AddCommand(cmd)
}
