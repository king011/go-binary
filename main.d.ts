declare namespace core {
    export const int16: Symbol
    export const int32: Symbol
    export const int64: Symbol
    export const uint16: Symbol
    export const uint32: Symbol
    export const uint64: Symbol
    export const float32: Symbol
    export const float64: Symbol
    export const byte: Symbol
    export const bool: Symbol
    export const string: Symbol

    export const int16s: Symbol
    export const int32s: Symbol
    export const int64s: Symbol
    export const uint16s: Symbol
    export const uint32s: Symbol
    export const uint64s: Symbol
    export const float32s: Symbol
    export const float64s: Symbol
    export const bytes: Symbol
    export const bools: Symbol
    export const strings: Symbol

    /**
     * 註冊一個新型別 並返回
     * @param pkg 型別所在包名    
     * @param typeName 型別名稱
     * 
     * @note pkg 忽略大小寫 , 只能是 以英文開始的 [英文 數字 /] 組合 , 使用  / 隔開子包 , 且 / 不能連續使用 不能位於 開始 和結尾
     * @note typeName 忽略大小寫 , 只能是 以英文開始的 [英文 數字 _] 組合 , 使用  _ 隔開關鍵字 , 且 _ 不能連續使用 不能位於 開始 和結尾
     */
    function RegisterType(pkg: string, typeName: string): Type;
    /**
     * 註冊一個新 枚舉型別 並返回 枚舉值只能是 int16
     * @param pkg 型別所在包名    
     * @param typeName 型別名稱
     * 
     * @note pkg 忽略大小寫 , 只能是 以英文開始的 [英文 數字 /] 組合 , 使用  / 隔開子包 , 且 / 不能連續使用 不能位於 開始 和結尾
     * @note typeName 忽略大小寫 , 只能是 以英文開始的 [英文 數字 _] 組合 , 使用  _ 隔開關鍵字 , 且 _ 不能連續使用 不能位於 開始 和結尾
     */
    function RegisterEnum(pkg: string, typeName: string): Enum;
    /**
     * 返回指定型別的 符號信息
     * @param pkg 包名
     * @param typeName 型別名
     * @param repeat 是否是數組
     * 
     * @note pkg 忽略大小寫 , 只能是 以英文開始的 [英文 數字 /] 組合 , 使用  / 隔開子包 , 且 / 不能連續使用 不能位於 開始 和結尾
     * @note typeName 忽略大小寫 , 只能是 以英文開始的 [英文 數字 _] 組合 , 使用  _ 隔開關鍵字 , 且 _ 不能連續使用 不能位於 開始 和結尾
     */
    function UseType(pkg: string, typeName: string, repeat?: boolean): Symbol;
    /**
     * 一個註冊的 型別
     */
    interface Type {
        /**
         * 爲型別 註冊 屬性
         * @param id 屬性 id 必須唯一 用於 序列化 屬性識別
         * @param name 屬性名 用於人類觀看 以及 生成的各語言內存結構對於 屬性名稱
         * @param symbol 屬性 符號信息
         * 
         * @note name 忽略大小寫, 只能是 以英文開始的 [英文 數字 _] 組合 , 使用  _ 隔開關鍵字 , 且 _ 不能連續使用 不能位於 開始 和結尾
         */
        register(id: number, name: string, symbol: Symbol): Type
        /**
         * 爲型別 註冊 屬性
         * @param field 屬性定義
         */
        register(...field: Array<Field>): Type
    }
    /**
     * 一個註冊的 型別
     */
    interface Enum {
        register(name: string, val?: number): Type
    }
    interface Field {
        /**
         * 屬性 id 必須唯一 用於 序列化 屬性識別
         */
        readonly id: number
        readonly name: string
        readonly Symbol: Symbol
    }
    /**
     * 定義 屬性 符號信息
     */
    interface Symbol {
        /**
         * 包名
         */
        readonly pkg: string
        /**
         * 型別名
         */
        readonly typeName: string
        /**
         * 是否 是 數組
         */
        readonly repeat: boolean
    }
}